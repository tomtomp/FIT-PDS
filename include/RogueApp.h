/**
 * @file RogueApp.h
 * @author Tomas Polasek
 * @brief Application class for the DHCP rogue server application.
 */

#ifndef PDS_DHCP_ROGUEAPP_H
#define PDS_DHCP_ROGUEAPP_H

#include "Types.h"
#include "Util.h"
#include "IpPool.h"
#include "SocketPcap.h"

/// Main class for the dhcp rogue server application.
class RogueApp
{
public:
    RogueApp(const RogueApp &other) = delete;
    RogueApp(RogueApp &&other) = delete;
    RogueApp operator=(const RogueApp &other) = delete;
    RogueApp operator=(RogueApp &&other) = delete;

    /// Initialize the application.
    RogueApp(int argc, char * const argv[]);
    RogueApp() = delete;

    /// Deallocate resources.
    ~RogueApp();

    /**
     * Run the configured application.
     * @return Returns return code for the whole program.
     */
    int run();
private:
    /// Argument parsing string.
    static constexpr const char *ARG_PARSE_OPTARG{"i:p:g:n:d:l:x:"};
    /// Help menu for the application.
    static constexpr const char *ARG_PARSE_USAGE{"Usage: ./pds-dhcprogue -i interface -p pool -g gateway -n "
                                                     "dns-server -d domain -l lease-time [-x xid]\n"
                                                     "\tinterface: Interface name used in the attack.\n"
                                                     "\tpool: Address pool used by the server. Specified in format <first_ip>-<last_ip.\n"
                                                     "\tgateway: Gateway address supplied to the clients.\n"
                                                     "\tdns-server: DNS server address supplied to the clients.\n"
                                                     "\tdomain: Domain name supplied to the clients.\n"
                                                     "\tlease-time: How long in seconds does a lease hold.\n"
                                                     "\txid: Optional parameter for setting message XID used by the starve application."};
    /// Maximal size of captured packets.
    static constexpr std::size_t BUFFER_SIZE{Util::EthHeader::size() + Util::IpHeader::size() +
                                             Util::UdpHeader::size() + Util::DhcpHeader::size()};
    /// Pcap filter used for getting packets targeted at DHCP server.
    static constexpr const char *DHCP_SERVER_FILTER{"dst port 67"};

    /// FSM stages the application goes through.
    enum class Stage
    {
        /// Waiting for packets aimed at DHCP server.
        Receive,
        /// Sending offer to the client.
        Offer,
        /// Sending acknowledge to the client.
        Ack,
        /// Sending not-acknowledged to the client.
        Nack,
        /// Finished all work.
        Finished
    }; // enum class Stages

    /**
     * Parse input parameter vector.
     * @param argc Length of the parameter vector.
     * @param argv Parameter vector itself.
     */
    void parseInputParams(int argc, char * const argv[]);

    /// Interface the rogue DHCP server will run on.
    std::string mTargetInterface;
    /// IP address pool specification.
    std::string mPoolSpec;
    /// Gateway provided to the clients.
    Util::Ipv4Address mGateway;
    /// DNS address provided to the clients.
    Util::Ipv4Address mDns;
    /// Domain name provided to the clients.
    std::string mDomain;
    /// For how long do we lease IP addresses.
    uint32_t mLeaseTime;
    /// Message XID used by the starve application.
    uint32_t mStarveXid;
protected:
}; // class RogueApp

#endif //PDS_DHCP_ROGUEAPP_H
