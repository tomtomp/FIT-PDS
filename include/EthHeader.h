/**
 * @file EthHeader.h
 * @author Tomas Polasek
 * @brief Ethernet header abstraction.
 */

#ifndef PDS_DHCP_ETHHEADER_H
#define PDS_DHCP_ETHHEADER_H

#include "Types.h"
#include <netinet/ether.h>

/// Namespace for user types and utility functions.
namespace Util
{
    /// Simple Ethernet header abstraction.
    class EthHeader
    {
    public:
        /// Initialize the Ethernet header to default state.
        EthHeader();

        EthHeader(const EthHeader &other) = default;
        EthHeader(EthHeader &&other) = default;
        EthHeader &operator=(const EthHeader &rhs) = default;
        EthHeader &operator=(EthHeader &&rhs) = default;

        /// Initialize the Ethernet header to default state.
        void initialize();

        /// Get pointer to the header data.
        const uint8_t *data() const
        { return reinterpret_cast<const uint8_t*>(&mEthHdr); }

        /// Get size of the header.
        static constexpr std::size_t size()
        { return HDR_SIZE; }

        /// Set source MAC address.
        void setSource(ether_addr &src);

        /// Set destination MAC address.
        void setDst(ether_addr &dst);
    private:
        /// Size of the IP header in bytes.
        static constexpr std::size_t HDR_SIZE{sizeof(ether_header)};

        /// Default source/destination address octet value.
        static constexpr uint8_t HDR_ADDR_OCTET{0u};
        /// Type of the Ethernet header -> IP.
        static constexpr uint16_t HDR_TYPE{ETHERTYPE_IP};

        /// Set the internal structure to all zeros.
        void zero();

        /// Get pointer to the internal data.
        uint8_t *rawData()
        { return reinterpret_cast<uint8_t*>(&mEthHdr); }

        /// Inner Ethernet header.
        ether_header mEthHdr;
    protected:
    }; // class UdpHeader
} // namespace Util

#endif //PDS_DHCP_ETHHEADER_H
