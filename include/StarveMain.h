/**
 * @file StarveMain.h
 * @author Tomas Polasek
 * @brief Contains main function for the dhcp-starve application.
 */

#ifndef PDS_DHCP_STARVEMAIN_H
#define PDS_DHCP_STARVEMAIN_H

#include "StarveApp.h"

#endif //PDS_DHCP_STARVEMAIN_H
