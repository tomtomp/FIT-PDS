/**
 * @file SocketRaw.h
 * @author Tomas Polasek
 * @brief Raw socket abstraction.
 */

#ifndef PDS_DHCP_SOCKETRAW_H_H
#define PDS_DHCP_SOCKETRAW_H_H

#include "Types.h"
#include "Util.h"
#include "IpAddress.h"
#include "Packet.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// Simple raw socket abstraction.
    class SocketRaw
    {
    public:
        /**
         * Create raw socket.
         * @throws std::runtime_error when unable to create the socket.
         */
        SocketRaw();
        /// Close the socket.
        ~SocketRaw();

        // No copying!
        SocketRaw(const SocketRaw &other) = delete;
        SocketRaw &operator=(const SocketRaw &rhs) = delete;
        // Destructive swap.
        SocketRaw(SocketRaw &&other);
        SocketRaw &operator=(SocketRaw &&rhs);

        /**
         * Create raw socket.
         * @throws std::runtime_error when unable to create the socket.
         */
        void create();

        /**
         * Set raw socket parameters and "bind" it to given interface.
         * @param interface Interface identifier.
         * @param inclIpHdr Tell whether we will provide our own IP header.
         * @param timeout Timeout of this socket in uSeconds, set to 0 for no timeout.
         * @throws std::runtime_error when unable to unable to bind to given interface.
         */
        void bindInterface(const std::string &interface, bool inclIpHdr, std::size_t timeout = TIMEOUT_U_SECONDS);

        /**
         * Send given packet using given addresses.
         * @param msg Packet to send.
         * @param to Target IP address, used for choosing for routing.
         * @param port Port number.
         */
        void sendTo(const Packet &msg, const Ipv4Address &to, in_port_t port);

        /**
         * Send given packet through given interface.
         * @param msg Packet to send.
         * @param interface Interface id.
         */
        void sendTo(const Packet &msg, std::uint32_t interface);

        /**
         * Receive a message from this socket and store it in given buffer.
         * @param buffer Storage for received data.
         */
        void rcv(std::vector<uint8_t> &buffer);

        /// Close created socket.
        void close();

        /// Is this socket ready for use?
        bool created() const
        { return mCreated; }
    private:
        /// Timeout in uSeconds.
        static constexpr std::size_t TIMEOUT_U_SECONDS{500000};
        /// Allow Ethernet frame specification.
        static constexpr int SOCK_DOMAIN{PF_PACKET};
        /// Raw socket.
        static constexpr int SOCK_TYPE{SOCK_RAW};
        /// Ethernet header.
        static constexpr int SOCK_PROTOCOL{ETH_P_ALL};

        /**
         * Swap this socket with another one.
         * @param other The other socket.
         */
        void swap(SocketRaw &&other);

        /// Is this socket created?
        bool mCreated{false};
        /// Handle to the socket.
        int mHandle{-1};
    protected:
    }; // class SocketRaw
} // namespace Util

#endif //PDS_DHCP_SOCKETRAW_H_H
