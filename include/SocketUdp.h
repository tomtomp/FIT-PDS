/**
 * @file SocketUdp.h
 * @author Tomas Polasek
 * @brief Udp socket abstractions.
 */

#ifndef PDS_DHCP_SOCKETUDP_H
#define PDS_DHCP_SOCKETUDP_H

#include "Types.h"
#include "Util.h"
#include "IpAddress.h"
#include "DhcpHeader.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// Simple udp socket abstraction.
    class SocketUdp
    {
    public:
        /**
         * Create UDP socket.
         * @throws std::runtime_error when unable to create the socket.
         */
        SocketUdp();
        /// Close the socket.
        ~SocketUdp();

        // No copying!
        SocketUdp(const SocketUdp &other) = delete;
        SocketUdp &operator=(const SocketUdp &rhs) = delete;
        // Destructive swap.
        SocketUdp(SocketUdp &&other);
        SocketUdp &operator=(SocketUdp &&rhs);

        /**
         * Create UDP socket.
         * @throws std::runtime_error when unable to create the socket.
         */
        void create();

        /**
         * Bind this socket to given interface.
         * @param interface Interface identifier.
         * @param port Port number.
         * @param timeout Timeout of this socket in uSeconds, set to 0 for no timeout.
         * @throws std::runtime_error when unable to unable to bind to given interface.
         */
        void bindInterface(const std::string &interface, in_port_t port, std::size_t timeout = TIMEOUT_U_SECONDS);

        /**
         * Send given packet in broadcast mode.
         * @param pkt Packet to send.
         * @param port Port number to send to.
         */
        void sendBroadcast(DhcpHeader &pkt, in_port_t port);

        /**
         * Receive a packet from any source.
         * @param buffer Buffer to hold the message.
         * @param port Port number to receive from.
         */
        void rcvBroadcast(std::vector<uint8_t> &buffer, in_port_t port);

        /// Close created socket.
        void close();

        /// Is this socket ready for use?
        bool created() const
        { return mCreated; }
    private:
        /// Timeout in uSeconds.
        static constexpr std::size_t TIMEOUT_U_SECONDS{500000};
        /// Use IP domain.
        static constexpr int SOCK_DOMAIN{AF_INET};
        /// UDP socket.
        static constexpr int SOCK_TYPE{SOCK_DGRAM};
        /// UDP protocol
        static constexpr int SOCK_PROTOCOL{IPPROTO_UDP};

        /**
         * Swap this socket with another one.
         * @param other The other socket.
         */
        void swap(SocketUdp &&other);

        /// Is this socket created?
        bool mCreated{false};
        /// Handle to the socket.
        int mHandle{-1};
    protected:
    }; // class SocketUdp
} // namespace Util

#endif //PDS_DHCP_SOCKETUDP_H
