/**
 * @file IpAddress.h
 * @author Tomas Polasek
 * @brief IP address abstraction.
 */

#ifndef PDS_DHCP_IPADDRESS_H
#define PDS_DHCP_IPADDRESS_H

#include "Types.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// Available types of IP addresses.
    enum class IpAddressType
    {
        IPV4,
        IPV6,
        SIZE
    }; // enum class IpAddressType

    /// Simple IP address wrapper around sockaddr_in# structure.
    class IpAddress
    {
    public:
        IpAddress() = default;
        virtual ~IpAddress() = default;

        /// Get type of the inner address.
        virtual sa_family_t saFamily() const = 0;

        /// Get type of the inner address.
        virtual IpAddressType family() const = 0;

        /// Print address to given output.
        virtual void print(std::ostream &stream) const = 0;
    private:
    protected:
    }; // class IpAddress

    class Ipv4Address : public IpAddress
    {
    public:
        /// Initialize the inner address to 0.0.0.0 .
        Ipv4Address();

        /**
         * Initialize the inner address from string.
         * @param addr Address specification.
         */
        Ipv4Address(const std::string &addr);

        /**
         * Initialize the inner address to given value.
         * @param addr
         */
        Ipv4Address(const in_addr &addr);

        /**
         * Initialize inner address from given network-endian value.
         * @param addr IP address in network endian.
         */
        Ipv4Address(uint32_t addr);

        /**
         * Initialize the IP address with given octets in form:
         *   o3.o2.o1.o0
         * @param o3 Highest octet.
         * @param o2 Second highest octet.
         * @param o1 Second lowest octet.
         * @param o0 lowest octet.
         */
        Ipv4Address(uint8_t o3, uint8_t o2, uint8_t o1, uint8_t o0);

        /**
         * Set inner address from string.
         * @param addr Address specification.
         */
        void setFrom(const std::string &addr);

        /**
         * Set inner address from given value.
         * @param addr Address to set to.
         */
        void setFrom(const in_addr &addr);

        /**
         * Initialize inner address from given network-endian value.
         * @param addr IP address in network endian.
         */
        void setFrom(uint32_t addr);

        /**
         * Set inner IP address with given octets in form:
         *   o3.o2.o1.o0
         * @param o3 Highest octet.
         * @param o2 Second highest octet.
         * @param o1 Second lowest octet.
         * @param o0 lowest octet.
         */
        void setFrom(uint8_t o3, uint8_t o2, uint8_t o1, uint8_t o0);

        virtual ~Ipv4Address() = default;

        /// Get type of the inner address.
        virtual sa_family_t saFamily() const override
        { return AF_INET; }

        /// Get type of the inner address.
        virtual IpAddressType family() const override
        { return IpAddressType::IPV4; }

        /// Print address to given output.
        virtual void print(std::ostream &stream) const override;

        /// Get pointer to the address.
        in_addr *address()
        { return &mAddress; }
        /// Get pointer to the address.
        const in_addr *address() const
        { return &mAddress; }

        /// Reset inner address to 0.0.0.0 .
        void reset();

        /// Get size of the inner structure returned by address.
        static constexpr std::size_t addressSize()
        { return sizeof(in_addr); }

        /**
         * Get given octet from the ip address.
         * @param num index of the octet, should be < 4!
         * @return Returns octet indexed by the given number or undefined value when num is >= 4.
         * @warning Returns uint16_t for easy printing, actual values are <0, 255>!
         */
        uint16_t getOctet(std::size_t num) const
        { return static_cast<uint16_t>((mAddress.s_addr >> (num * BITS_IN_BYTE)) & 0xFF); }

        /// Get address as a single number in network endian.
        uint32_t getAddr() const
        { return mAddress.s_addr; }

        /// Compare IP addresses
        bool operator==(const Ipv4Address &other) const
        { return getAddr() == other.getAddr(); }
        /// Compare IP addresses
        bool operator!=(const Ipv4Address &other) const
        { return !(*this == other); }
    private:
        /// Number of bits in a byte.
        static constexpr std::size_t BITS_IN_BYTE{8u};

        /// Inner address holder.
        in_addr mAddress;
    protected:
    };

    /// Print given address.
    std::ostream &operator<<(std::ostream &stream, const Ipv4Address &address);
} // namespace Util

#endif //PDS_DHCP_IPADDRESS_H
