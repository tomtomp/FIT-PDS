/**
 * @file IpPool.h
 * @author Tomas Polasek
 * @brief Pool of IP addresses.
 */

#ifndef PDS_DHCP_IPPOOL_H
#define PDS_DHCP_IPPOOL_H

#include "Types.h"
#include "Util.h"
#include "IpAddress.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// IP address pool.
    class IpPool
    {
    public:
        /// Default lease time for leasing IP addresses in seconds.
        static constexpr std::size_t DEFAULT_LEASE_TIME{3600u};

        /**
         * Construct the pool from formatted string in following shape:
         *   <first_ip>-<last_ip>
         * @param range Formatted string specifying the IP addresses in the pool.
         */
        IpPool(const std::string &range);

        /**
         * Construct the pool from starting and ending IP address.
         * @param first First IP address in the pool.
         * @param last Last IP address in the pool.
         */
        IpPool(const Util::Ipv4Address &first, const Util::Ipv4Address &last);

        // Default copy/move constructor and assignment operators.
        IpPool(const IpPool &other) = default;
        IpPool(IpPool &&other) = default;
        IpPool &operator=(const IpPool &other) = default;
        IpPool &operator=(IpPool &&other) = default;

        /**
         * Initialize the pool from formatted string in following shape:
         *   <first_ip>-<last_ip>
         * @param range Formatted string specifying the IP addresses in the pool.
         */
        void initialize(const std::string &range);

        /**
         * Initialize the pool.
         * @param first First IP address in the pool.
         * @param last Last IP address in the pool.
         */
        void initialize(const Util::Ipv4Address &first, const Util::Ipv4Address &last);

        /**
         * Initialize the pool.
         * @param first First IP address in the pool.
         * @param last Last IP address in the pool.
         */
        void initialize(uint32_t first, uint32_t last);

        /**
         * Reset the leases with the same first and last IP address.
         */
        void reset();

        /**
         * Set lease time for the leased IP addresses.
         * @param leaseTime The new lease time in seconds.
         */
        void setLeaseTime(std::size_t leaseTime);

        /**
         * Lease ip for given MAC address.
         * @param mac MAC address of the client.
         * @return Returns IP address leased to the MAC address.
         */
        Util::Ipv4Address leaseIp(const ether_addr &mac);

        /**
         * Lease ip for given MAC address. Attempts to lease the
         * preferred IP address first.
         * @param mac MAC address of the client.
         * @param preferred IP address preferred by the client.
         * @return Returns IP address leased to the MAC address.
         */
        Util::Ipv4Address leaseIp(const ether_addr &mac, const Util::Ipv4Address &preferred);

        /**
         * Renew lease for specified IP address.
         * @param ip IP address to be re-leased.
         */
        void renewIp(const Util::Ipv4Address &ip);

        /**
         * Check if given IP address belongs to specified device.
         * @param ip IP address to check for.
         * @param mac MAC address representing the device.
         * @return Returns true, if the IP address does belong to given device.
         */
        bool ipBelongsTo(const Util::Ipv4Address &ip, const ether_addr &mac) const;

        /**
         * Is the given IP address free for use?
         * @param ip IP address queried.
         * @return Returns true if the IP address is free for use.
         */
        bool ipFree(const Util::Ipv4Address &ip) const
        { return ipFree(ntohl(ip.getAddr())); }

        /**
         * Release specified IP address.
         * @param ip IP address to be released.
         */
        void releaseIp(const Util::Ipv4Address &ip);

        /**
         * Print leased IP addresses.
         * @param printExpired Should expired leases be printed too?
         */
        void printLeased(bool printExpired) const;
    private:
        /// Clock used for measuring the lease time.
        using LeaseClock = std::chrono::system_clock;
        /// Time point data type.
        using LeaseTimePoint = LeaseClock::time_point;
        /// Record for leased IP address.
        struct LeaseRecord
        {
            /// Number of milliseconds in one second.
            static constexpr std::size_t MS_IN_S{1000u};

            /// MAC address of the device which has leased this IP address.
            ether_addr mac;
            /// Time of the lease.
            LeaseTimePoint leaseTime;

            /// Get time in seconds for how long has the IP address been leased for.
            float leasedFor() const
            {
                return std::chrono::duration_cast<std::chrono::milliseconds>(LeaseClock::now() - leaseTime).count()
                     / static_cast<float>(MS_IN_S);
            }

            /// Renew time in this record.
            void renewRecord()
            { leaseTime = LeaseClock::now(); }
        }; // struct LeaseRecord
        /// Record for lease from MAC address.
        struct MacLeaseRecord
        {
            /// IP address leased by the MAC address.
            uint32_t ip;
        }; // struct MacLeaseRecord

        /// Move to the next IP address.
        void nextIp();

        /**
         * Inner method for setting the records straight.
         * @param mac MAC address to set the IP for.
         * @param ip IP address to set the MAC for.
         */
        void innerLeaseIp(const ether_addr &mac, uint32_t ip);

        /**
         * Is the given IP address free for use?
         * @param ip IP address queried.
         * @return Returns true if the IP address is free for use.
         */
        bool ipFree(uint32_t ip) const;

        /// How long are the addresses leased for.
        std::size_t mLeaseTime{DEFAULT_LEASE_TIME};
        /// Map of leases from IP address.
        std::map<uint32_t, LeaseRecord> mLeasesIp;
        /// Map of leases from MAC address.
        std::map<ether_addr, MacLeaseRecord> mLeasesMac;
        /// First IP address in the pool.
        uint32_t mFirstIp;
        /// Last IP address in the pool.
        uint32_t mLastIp;
        /// Next IP to be used.
        uint32_t mCurrentIp;
    protected:
    }; // class IpPool
} // namespace Util

#endif //PDS_DHCP_IPPOOL_H
