/**
 * @file IpHeader.h
 * @author Tomas Polasek
 * @brief IP header abstraction.
 */

#ifndef PDS_DHCP_IPHEADER_H
#define PDS_DHCP_IPHEADER_H

#include "Types.h"
#include "Util.h"
#include <netinet/ip.h>

#include "IpAddress.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// Simple IP header abstraction.
    class IpHeader
    {
    public:
        /// Initialize the IP header to default state.
        IpHeader();

        IpHeader(const IpHeader &other) = default;
        IpHeader(IpHeader &&other) = default;
        IpHeader &operator=(const IpHeader &rhs) = default;
        IpHeader &operator=(IpHeader &&rhs) = default;

        /// Initialize the IP header to default state.
        void initialize();

        /**
         * Get pointer to the IP header. May need to recalculate the checksum!
         * @return Returns pointer to the IP header.
         */
        const uint8_t *data();

        /**
         * Get pointer to the IP header. If checksum recalculation is required, throws!
         * @return Returns pointer to the IP header.
         * @throws std::runtime_error If recalculation of checksum is required.
         */
        const uint8_t *data() const;

        /// Get size of the header.
        static constexpr std::size_t size()
        { return HDR_SIZE; }

        /// Re-calculate the IP checksum.
        void recalcChecksum();

        /**
         * Set length of the payload without the IP header.
         * @param len Length of the payload.
         */
        void setPayloadLength(uint16_t len);

        /// Set ID of the packet.
        void setId(uint16_t id);

        /// Set next-header protocol.
        void setProto(uint8_t proto);

        /// Set source IP address.
        void setSource(const Ipv4Address &src);

        /// Set destination IP address.
        void setDst(const Ipv4Address &dst);
    private:
        /// Size of the IP header in bytes.
        static constexpr std::size_t HDR_SIZE{sizeof(iphdr)};

        /// Minimal size, no optional headers.
        static constexpr uint8_t HDR_IHL{5u};
        /// IPv4 header.
        static constexpr uint8_t HDR_VERSION{4u};
        /// TOS unused.
        static constexpr uint8_t HDR_TOS{0u};
        /// Starting length set to 0, needs to be set by user.
        static constexpr uint16_t HDR_TOTAL_LENGTH{0u};
        /// Default packet ID.
        static constexpr uint16_t HDR_ID{0x1337};
        /// No fragmentation.
        static constexpr uint16_t HDR_FRAG_OFFSET{0u};
        /// Maximal time to live.
        static constexpr uint8_t HDR_TTL{255u};
        /// Default protocol is UDP.
        static constexpr uint8_t HDR_PROTO{IPPROTO_UDP};
        /// Checksum before computation has to be set to 0!
        static constexpr uint16_t HDR_CHECK{0u};
        /// Source/destination address defaults to 0.0.0.0 .
        static constexpr uint32_t HDR_ADDR{0u};

        /// Set the internal structure to all zeros.
        void zero();

        /// Get pointer to the internal data without recalculating checksum.
        uint8_t *rawData()
        { return reinterpret_cast<uint8_t*>(&mIpHdr); }

        /// Inner IP header.
        iphdr mIpHdr;
        /// Dirty flag for recalculating checksum.
        bool mDirty;
    protected:
    };
} // namespace Util

#endif //PDS_DHCP_IPHEADER_H
