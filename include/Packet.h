/**
 * @file Packet.h
 * @author Tomas Polasek
 * @brief IP packet abstraction.
 */

#ifndef PDS_DHCP_PACKET_H
#define PDS_DHCP_PACKET_H

#include "Types.h"
#include "EthHeader.h"
#include "IpHeader.h"
#include "UdpHeader.h"
#include "DhcpHeader.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// IP packet abstraction.
    class Packet
    {
    public:
        /**
         * Construct the final packet from given headers.
         * @param eth Ethernet header.
         * @param ip IP header. Sets the payload length.
         * @param udp UDP header. Sets the payload length.
         * @param dhcp DHCP header, adds the finishing 0xff byte.
         */
        Packet(EthHeader &eth, IpHeader &ip, UdpHeader &udp, DhcpHeader &dhcp);

        Packet(const Packet &other) = default;
        Packet(Packet &&other) = default;
        Packet &operator=(const Packet &rhs) = default;
        Packet &operator=(Packet &&rhs) = default;

        /**
         * Construct the final packet from given headers.
         * @param eth Ethernet header.
         * @param ip IP header. Sets the payload length.
         * @param udp UDP header. Sets the payload length.
         * @param dhcp DHCP header, adds the finishing 0xff byte.
         */
        void build(EthHeader &eth, IpHeader &ip, UdpHeader &udp, DhcpHeader &dhcp);

        /**
         * Replace the DHCP header in already constructed packet.
         * @param dhcp DHCP header, adds the finishing 0xff byte.
         */
        void replace(DhcpHeader &dhcp);

        /// Get pointer to the packet data.
        const uint8_t *data() const
        { return mMsg.data(); }

        /// Get size of the packet in bytes.
        std::size_t size() const
        { return mMsg.size(); }
    private:
        /// Get pointer to the date.
        uint8_t *rawData()
        { return mMsg.data(); }

        /// Packet data.
        std::vector<uint8_t> mMsg;
        /// Position of the start of the DHCP header.
        std::size_t mDhcpPos;
    protected:
    }; // class Packet
} // namespace Util

#endif //PDS_DHCP_PACKET_H
