/**
 * @file DhcpHeader.h
 * @author Tomas Polasek
 * @brief DHCP header abstraction. Used for creation and parsing of DHCP packets.
 */

#ifndef PDS_DHCP_DHCPHEADER_H
#define PDS_DHCP_DHCPHEADER_H

#include "Types.h"
#include "Util.h"
#include "IpAddress.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// Namespace containing packet data structures.
    namespace PktData
    {
        /// DHCP operation codes.
        enum DhcpOp
        {
            /// Request message.
            REQUEST = 1,
            /// Reply message.
            REPLY = 2
        }; // enum DhcpOp

        /// Size of the client hardware address array.
        static constexpr std::size_t CHADDR_ARR_SIZE{16u};
        /// Size of the server name array.
        static constexpr std::size_t SNAME_ARR_SIZE{64u};
        /// Size of the boot filename array.
        static constexpr std::size_t FILE_ARR_SIZE{128u};

        /// Maximal size of the options array.
        static constexpr std::size_t OPTIONS_ARR_SIZE{312u};

        /// Hardware type identifier for Ethernet.
        static constexpr uint8_t HTYPE_ETHERNET{1u};

        /// Length of hardware address used by Ethernet.
        static constexpr std::size_t HLEN_ETHERNET{6u};

        /// Flag specifying that client doesnt yet know its IP address.
        static constexpr uint16_t FLAG_BROADCAST{0x0080};

        /// Magic cookie for the options array : https://tools.ietf.org/html/rfc2132.
        static constexpr uint8_t MAGIC_COOKIE[4] = { 0x63, 0x82, 0x53, 0x63 };

        /// DHCP option for specifying message type.
        static constexpr uint8_t DHCP_OPTION_MSG_TYPE_ID{53u};
        static constexpr uint8_t DHCP_OPTION_MSG_TYPE_LEN{1u};
        enum DhcpMsgTypeOptions
        {
            DHCPDISCOVER = 1,
            DHCPOFFER = 2,
            DHCPREQUEST = 3,
            DHCPDECLINE = 4,
            DHCPACK = 5,
            DHCPNAK = 6,
            DHCPRELEASE = 7,
            DHCPINFORM = 8,
            LAST,
        };
        /// DHCP option for specifying requested IP address.
        static constexpr uint8_t DHCP_OPTION_ADDR_REQ_ID{50u};
        static constexpr uint8_t DHCP_OPTION_ADDR_REQ_LEN{4u};

        /// DHCP option for specifying DHCP server IP address.
        static constexpr uint8_t DHCP_OPTION_DHCP_SERV_ID{54u};
        static constexpr uint8_t DHCP_OPTION_DHCP_SERV_LEN{4u};

        /// DHCP option for specifying requested paramaters to the DHCP server.
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_ID{55u};
        /// Length of a single IPv4 address in bytes.
        static constexpr uint8_t DHCP_OPTION_IP_ADDR_LEN{4u};
        // Codes found from standard DHCP communication in Wireshark...
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_SUBNET_MASK{1u};
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_LEASE_TIME{51u};
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_BROADCAST{28u};
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_TIME_OFFSET{2u};
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_ROUTER{3u};
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_DOMAIN_NAME{15u};
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_DNS{6u};
        static constexpr uint8_t DHCP_OPTION_PARAM_REQ_HOST_NAME{12u};

        /// DHCP Packet structure, found on: http://www.tcpipguide.com/free/t_DHCPMessageFormat.htm .
        struct DhcpMessage
        {
            /// Operation code, specifies type of this message.
            uint8_t op;
            /// Type of hardware used by local network.
            uint8_t htype;
            /// Length of address for used hardware.
            uint8_t hlen;
            /// Number of hops, set to 0 by client before sending request.
            uint8_t hops;

            /// Session identifier.
            uint32_t xid;
            /// Seconds elapsed since beginning of communication.
            uint16_t secs;
            /// Flag bit array.
            uint16_t flags;

            static_assert(sizeof(in_addr) == sizeof(uint32_t), "Size of IPv4 address should be 4 bytes!");

            /// Client IP address.
            in_addr ciAddr;
            /// Your IP address. Assigned by server.
            in_addr yiAddr;
            /// IP address of the server.
            in_addr siAddr;
            /// Gateway IP address.
            in_addr giAddr;

            /// Client hardware address (layer 2).
            uint8_t chAddr[CHADDR_ARR_SIZE];
            /// Server name.
            uint8_t sName[SNAME_ARR_SIZE];
            /// Boot filename.
            uint8_t fileName[FILE_ARR_SIZE];

            /// Variable length array of options.
            uint8_t options[OPTIONS_ARR_SIZE];

            // End must be signalled by 0xFF byte.
        }; // struct DhcpMessage
    } // namespace Packets.

    /// Type of DHCP messages.
    enum class DhcpMessageType
    {
        Request,
        Reply,
        Unknown
    }; // enum class DhcpMessageType

    /// Type of network hardware.
    enum class DhcpHardwareType
    {
        Ethernet,
        Unknown
    }; // enum class DhcpHardwareType

    /// Type of DHCP message in the options array.
    enum class DhcpOptionType : uint8_t
    {
        Discover = 1,
        Offer = 2,
        Request = 3,
        Decline = 4,
        Ack = 5,
        Nack = 6,
        Release = 7,
        Inform = 8,
        Unknown
    }; // enum class DhcpOptionType

    /// Information extracted from a DHCP message.
    struct DhcpParsedMessage
    {
        /// Type of the message.
        DhcpMessageType type;
        /// Type of the network hardware.
        DhcpHardwareType hType;
        /// Length of the hardware address.
        uint8_t hAddrLen;
        /// Number of hops.
        uint8_t hops;

        /// Session identifier.
        uint32_t xid;
        /// Number of seconds since beginning of communication.
        uint16_t secs;
        /// Has the broadcast flag been set?
        bool broadcastFlag;

        /// Client IP address.
        Util::Ipv4Address ciAddr;
        /// Your IP address. Assigned by server.
        Util::Ipv4Address yiAddr;
        /// IP address of the server.
        Util::Ipv4Address siAddr;
        /// Gateway IP address.
        Util::Ipv4Address giAddr;

        /// MAC address (Only Ethernet is supported).
        ether_addr macAddr;

        /// Actual type of the DHCP message.
        DhcpOptionType optType;

        /// Server address specified in OFFER message.
        Ipv4Address serverAddr;
        /// IP address requested by DHCP client.
        Ipv4Address requestedAddr;

        /// Reset optional settings to default value.
        void resetOptions();
    }; // struct DhcpParsedMessage

    /// DHCP header abstraction.
    class DhcpHeader
    {
    public:
        /**
         * Parse given DHCP packet data.
         * @param data Pointer to the packet data.
         * @param startIdx Index where to start the parsing at.
         * @warning Size of data should be at most bufferSize() .
         * @throws std::runtime_error If the parsing fails.
         */
        DhcpHeader(const std::vector<uint8_t> &data, std::size_t startIdx = 0);

        /**
         * Parse given DHCP packet data.
         * @param data Pointer to the packet data.
         * @param startIdx Index where to start the parsing at.
         * @warning Size of data should be at most bufferSize() .
         * @throws std::runtime_error If the parsing fails.
         */
        void loadFrom(const std::vector<uint8_t> &data, std::size_t startIdx = 0);

        /// Prepare empty DHCP message ready for setting operations.
        DhcpHeader();

        /// Reset the message to default "empty" state.
        void reset();

        /// Set message type.
        void setMsgType(DhcpMessageType type);
        /// Set option type.
        void setOptType(DhcpOptionType type);
        /// Set number of hops.
        void setHops(uint8_t hops);
        /// Set session ID.
        void setXid(uint32_t xid);
        /// Set seconds elapsed.
        void setSecs(uint16_t secs);
        /// Set broadcast flag.
        void setBroadcast(bool flag);
        /// Set client IP address.
        void setCiAddr(const Ipv4Address &addr);
        /// Set "your" IP address.
        void setYiAddr(const Ipv4Address &addr);
        /// Set server IP address.
        void setSiAddr(const Ipv4Address &addr);
        /// Set gateway IP address.
        void setGiAddr(const Ipv4Address &addr);
        /// Set hardware address.
        void setHAddr(const ether_addr &addr);

        /**
         * Push an option for requesting IP address to the end of the options array.
         * @return Returns index which can be used to set the IP address at a later date.
         */
        std::size_t pushOptAddrReq();
        /**
         * Push an option for specifying DHCP server to the end of the options array.
         * @return Returns index which can be used to set the IP address at a later date.
         */
        std::size_t pushOptServerId();

        /**
         * Push required information to the end of the options array.
         * @param req List of configuration parameters requested from DHCP server.
         */
        void pushOptRequestList(const std::initializer_list<uint8_t> &req);

        /**
         * Set IP address on given position, which should have been returned by one of the
         * pushOpt* functions.
         * @param idx Index to put the IP address at.
         * @param addr Address to copy over.
         * @throw std::runtime_error on error.
         */
        void setOptIp(std::size_t idx, const Ipv4Address &addr);

        /**
         * Push length of lease into the options array.
         * @param leaseTime For how long we lease IP addresses.
         */
        void pushOptLeaseTime(uint32_t leaseTime);

        /**
         * Push subnet mask into the options array.
         * @param mask Mask value.
         */
        void pushOptSubnetMask(const Util::Ipv4Address &mask);

        /**
         * Push gateway address into the options array.
         * @param addr Gateway IP address.
         */
        void pushOptRouter(const Util::Ipv4Address &addr);

        /**
         * Push DNS address into the options array.
         * @param addr Address of the DNS.
         */
        void pushOptDns(const Util::Ipv4Address &addr);

        /**
         * Push domain name into the options array.
         * @param name Domain name.
         */
        void pushOptDomainName(const std::string &name);

        /// Get size of buffer required for receiving DHCP messages.
        static constexpr std::size_t size()
        { return sizeof(PktData::DhcpMessage); }

        /**
         * Write the finishing 0xff and get pointer to the data.
         * @return Returns pointer to the packet data.
         */
        const uint8_t *getFinishBuffer();

        /// Get information about parsed DHCP message.
        const DhcpParsedMessage &info() const
        { return mParsed; }

    private:
        /// Position of the message type in the options array.
        static constexpr std::size_t MESSAGE_TYPE_POS{6u};

        /**
         * Copy given data vector to inner structure.
         * @param startIdx Index to start the copying at.
         * @param data Data vector.
         */
        void copyData(const std::vector<uint8_t> &data, std::size_t startIdx);

        /**
         * Parse inner message structure.
         */
        void parseMessage();

        /**
         * Parse the options array.
         */
        void parseOptions();

        /// Push a new option with given message type.
        void pushOptMsgType(uint8_t msgType);
        /// Set the message type option.
        void setOptMsgType(uint8_t msgType);

        /**
         * Push value of given type to options array.
         * @tparam T Type of the value.
         * @param val Value to be pushed.
         * @warning Automatically increments the mUsedOptionBytes!
         */
        template <typename T>
        void pushOption(const T &val);

        /**
         * Get reference to a value of given type in the memory.
         * @tparam T Type of the value.
         * @param pos Position of the value in the memory.
         * @return Returns reference to the value.
         */
        template <typename T>
        T &readOption(uint8_t *pos);
        /**
         * Get reference to a value of given type in the memory.
         * @tparam T Type of the value.
         * @param pos Position of the value in the options array.
         * @return Returns reference to the value.
         */
        template <typename T>
        T &readOption(std::size_t pos);

        /// Memory of the packet.
        PktData::DhcpMessage mMessage;
        /// Number of bytes used of the options array.
        std::size_t mUsedOptionBytes;
        /// Parsed information.
        DhcpParsedMessage mParsed;
    protected:
    }; // class DhcpHeader

    // Template implementation.

    template <typename T>
    void DhcpHeader::pushOption(const T &val)
    {
        auto size = sizeof(T);
        if (mUsedOptionBytes + size > PktData::OPTIONS_ARR_SIZE)
        {
            throw std::runtime_error("Unable to push value to options : Overflow!");
        }

        (*reinterpret_cast<T*>(mMessage.options + mUsedOptionBytes)) = val;
        mUsedOptionBytes += size;
    }

    template <typename T>
    T &DhcpHeader::readOption(uint8_t *pos)
    {
        return *reinterpret_cast<T*>(pos);
    }

    template <typename T>
    T &DhcpHeader::readOption(std::size_t pos)
    {
        if (mUsedOptionBytes < pos + sizeof(T))
        {
            throw std::runtime_error("Cannot read variable in options, value out of bounds!");
        }
        return readOption<T>(mMessage.options + pos);
    }
} // namespace Util

/// Print operator for DHCP message type.
std::ostream &operator<<(std::ostream &out, Util::DhcpMessageType type);

/// Print operator for DHCP hardware type.
std::ostream &operator<<(std::ostream &out, Util::DhcpHardwareType type);

/// Print operator for DHCP option type.
std::ostream &operator<<(std::ostream &out, Util::DhcpOptionType type);

/// Print operator for DHCP parsed message.
std::ostream &operator<<(std::ostream &out, const Util::DhcpParsedMessage &msg);

#endif //PDS_DHCP_DHCPHEADER_H
