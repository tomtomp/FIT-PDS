/**
 * @file Types.h
 * @author Tomas Polasek
 * @brief Common types for both applications.
 */

#ifndef PDS_DHCP_TYPES_H
#define PDS_DHCP_TYPES_H


// Standard libraries.
#include <iostream>
#include <memory>
#include <exception>
#include <vector>
#include <string>
#include <cstring>
#include <iomanip>
#include <map>
#include <chrono>
#include <random>
#include <sstream>

// System includes.
#include <unistd.h>
#include <signal.h>

// Networking includes
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
//#include <net/if_packet.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <netinet/if_ether.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
// getnameinfo(...)
#include <netdb.h>

/// Namespace for user types and utility functions.
namespace Util
{

} // namespace Util

#endif //PDS_DHCP_TYPES_H
