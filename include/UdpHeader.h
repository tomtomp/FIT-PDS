/**
 * @file UdpHeader.h
 * @author Tomas Polasek
 * @brief Udp header abstraction.
 */

#ifndef PDS_DHCP_UDPHEADER_H
#define PDS_DHCP_UDPHEADER_H

#include "Types.h"
#include <netinet/udp.h>

/// Namespace for user types and utility functions.
namespace Util
{
    /// Simple UDP header abstraction.
    class UdpHeader
    {
    public:
        /// Initialize the UDP header to default state.
        UdpHeader();

        UdpHeader(const UdpHeader &other) = default;
        UdpHeader(UdpHeader &&other) = default;
        UdpHeader &operator=(const UdpHeader &rhs) = default;
        UdpHeader &operator=(UdpHeader &&rhs) = default;

        /// Initialize the UDP header to default state.
        void initialize();

        /// Get pointer to the header data.
        const uint8_t *data() const
        { return reinterpret_cast<const uint8_t*>(&mUdpHdr); }

        /// Get size of the header.
        static constexpr std::size_t size()
        { return HDR_SIZE; }

        /**
         * Set length of the payload without the UDP header.
         * @param len Length of the payload.
         */
        void setPayloadLength(uint16_t len);

        /// Set source UDP port.
        void setSource(uint16_t src);

        /// Set destination UDP port.
        void setDst(uint16_t dst);
    private:
        /// Size of the IP header in bytes.
        static constexpr std::size_t HDR_SIZE{sizeof(udphdr)};

        /// Default source/destination port.
        static constexpr uint16_t HDR_PORT{0u};
        /// Starting length set to 0, needs to be set by user.
        static constexpr uint16_t HDR_TOTAL_LENGTH{0u};
        /// Checksum for IPv4 is optional -> set to 0 to specify it is unused.
        static constexpr uint16_t HDR_CHECK{0u};

        /// Set the internal structure to all zeros.
        void zero();

        /// Get pointer to the internal data.
        uint8_t *rawData()
        { return reinterpret_cast<uint8_t*>(&mUdpHdr); }

        /// Inner UDP header.
        udphdr mUdpHdr;
    protected:
    }; // class UdpHeader
} // namespace Util

#endif //PDS_DHCP_UDPHEADER_H
