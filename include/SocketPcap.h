/**
 * @file SocketPcap.h
 * @author Tomas Polasek
 * @brief Wrapper around pcap handle for ease of use.
 */

#ifndef PDS_DHCP_SOCKETPCAP_H
#define PDS_DHCP_SOCKETPCAP_H

#include "Types.h"
#include "Util.h"
#include "IpAddress.h"
#include "Packet.h"

#include <pcap/pcap.h>

/// Namespace for user types and utility functions.
namespace Util
{
    /// Simple wrapper around PCAP handle.
    class SocketPcap
    {
    public:
        /// Empty handle.
        SocketPcap() = default;

        /**
         * Create PCAP handle.
         * @param interface Interface to bind the handle to.
         * @param bufferSize Maximal size of received message.
         * @param timeout Timeout of the socket. Set to 0 for no timeout.
         * @throws std::runtime_error when unable to create the handle.
         */
        SocketPcap(const std::string &interface, std::size_t bufferSize, std::size_t timeout = TIMEOUT_M_SECONDS);
        /// Close the PCAP handle.
        ~SocketPcap();

        // No copying!
        SocketPcap(const SocketPcap &other) = delete;
        SocketPcap &operator=(const SocketPcap &rhs) = delete;
        // Destructive swap.
        SocketPcap(SocketPcap &&other);
        SocketPcap &operator=(SocketPcap &&rhs);

        /**
         * Create PCAP handle.
         * @param interface Interface to bind the handle to.
         * @param bufferSize Maximal size of received message.
         * @param timeout Timeout of the socket. Set to 0 for no timeout.
         * @throws std::runtime_error when unable to create the handle.
         */
        void create(const std::string &interface, std::size_t bufferSize, std::size_t timeout = TIMEOUT_M_SECONDS);

        /**
         * Set filter from source code.
         * @param filter Filter source code.
         * @param optimize Should the filter be optimized?
         * @param mask Mask used for determining, whether broadcast packet should be accepted.
         */
        void setFilter(const std::string &filter, bool optimize, const Util::Ipv4Address &mask);

        /**
         * Send given packet through bound interface.
         * @param msg Packet to send.
         */
        void send(const Packet &msg);

        /**
         * Receive a message from this socket and store it in given buffer.
         * @param buffer Storage for received data.
         */
        void rcv(std::vector<uint8_t> &buffer);

        /**
         * Get network and mask for given interface.
         * @param interface Interface to get information for.
         * @param net Output for network address.
         * @param mask Output for mask.
         */
        static void getIntNetMask(const std::string &interface, Util::Ipv4Address &net, Util::Ipv4Address &mask);

        /// Close created socket.
        void close();

        /// Is this socket ready for use?
        bool created() const
        { return mHandle != nullptr; }
    private:
        /// Timeout in mSeconds.
        static constexpr std::size_t TIMEOUT_M_SECONDS{500};

        /**
         * Swap this socket with another one.
         * @param other The other socket.
         */
        void swap(SocketPcap &&other);

        /// PCAP handle..
        pcap_t *mHandle{nullptr};
    protected:
    }; // class SocketPcap
} // namespace Util

#endif //PDS_DHCP_PCAPSOCKET_H
