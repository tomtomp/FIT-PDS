/**
 * @file StarveApp.h
 * @author Tomas Polasek
 * @brief Application class for the dhcp starve application.
 */

#ifndef PDS_DHCP_STARVEAPP_H
#define PDS_DHCP_STARVEAPP_H

#include "Types.h"
#include "Util.h"
#include "Timer.h"
#include "IpAddress.h"
#include "EthHeader.h"
#include "IpHeader.h"
#include "UdpHeader.h"
#include "DhcpHeader.h"
#include "SocketUdp.h"
#include "SocketPcap.h"

/// Main class for the dhcp starve application.
class StarveApp
{
public:
    StarveApp(const StarveApp &other) = delete;
    StarveApp(StarveApp &&other) = delete;
    StarveApp operator=(const StarveApp &other) = delete;
    StarveApp operator=(StarveApp &&other) = delete;

    /// Initialize the application.
    StarveApp(int argc, char * const argv[]);
    StarveApp() = delete;

    /// Deallocate resources.
    ~StarveApp();

    /**
     * Run the configured application.
     * @return Returns return code for the whole program.
     */
    int run();
private:
    /// Argument parsing string.
    static constexpr const char *ARG_PARSE_OPTARG{"i:x:"};
    /// Help menu for the application.
    static constexpr const char *ARG_PARSE_USAGE{"Usage: ./pds-dhcpstarve -i interface [-x xid]"
                                                     "\n\tinterface: Interface name used in the attack."
                                                     "\n\txid: Optional parameter for setting message XID."};
    /// Maximal size of captured packets.
    static constexpr std::size_t BUFFER_SIZE{Util::EthHeader::size() + Util::IpHeader::size() +
                                             Util::UdpHeader::size() + Util::DhcpHeader::size()};
    /// Pcap filter used for getting packets targeted at DHCP client.
    static constexpr const char *DHCP_CLIENT_FILTER{"dst port 68"};
    /// Maximum number of waits for offer message.
    static constexpr std::size_t MAX_OFFER_WAIT{5u};
    /// Maximum number of waits for acknowledge message.
    static constexpr std::size_t MAX_ACK_WAIT{5u};
    /// Maximum number of discovers sent before quitting.
    static constexpr std::size_t MAX_DISCOVERS{5u};
    /// Wait time before sending next discover in milliseconds.
    static constexpr std::size_t DISCOVER_WAIT_MS{300u};
    /// Default value for the XID.
    static constexpr uint32_t DEFAULT_XID{0x1337};

    /// FSM stages the application goes through.
    enum class Stage
    {
        /// Sending the discover message.
        Discover,
        /// Waiting for offer from DHCP server.
        Offer,
        /// Sending request message.
        Request,
        /// Waiting for acknowledge from DHCP server.
        Ack,
        /// Waiting before sending next discover.
        Wait,
        /// No more addresses to request.
        Finished
    }; // enum class Stages

    /**
     * Parse input parameter vector.
     * @param argc Length of the parameter vector.
     * @param argv Parameter vector itself.
     */
    void parseInputParams(int argc, char * const argv[]);

    /// Interface targeted by the application.
    std::string mTargetInterface;
    /// XID used for all messages.
    uint32_t mXid;
protected:
}; // class StarveApp

#endif //PDS_DHCP_STARVEAPP_H
