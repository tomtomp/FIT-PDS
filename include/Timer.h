/**
 * @file Timer.h
 * @author Tomas Polasek
 * @brief Simple std::chrono powered timer.
 */

#ifndef PDS_DHCP_TIMER_H
#define PDS_DHCP_TIMER_H

#include "Types.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /// Simple std::chrono powered timer.
    class Timer
    {
    public:
        /// Start the timer.
        Timer()
        { start(); }

        // Default copy/move constructor and assignment operators.
        Timer(const Timer &other) = default;
        Timer(Timer &&other) = default;
        Timer &operator=(const Timer &rhs) = default;
        Timer &operator=(Timer &&rhs) = default;

        /// Start the timer.
        void start()
        { mStart = TimerClock::now(); }

        /// Get number of elapsed milliseconds since the start of the timer.
        std::size_t elapsedMs() const
        { return std::max<int64_t>(std::chrono::duration_cast<std::chrono::milliseconds>(now() - mStart).count(), 0); }
    private:
        using TimerClock = std::chrono::system_clock;
        using TimerTimePoint = TimerClock::time_point;

        /// Get current time according to the clock used by this timer.
        static TimerTimePoint now()
        { return TimerClock::now(); }

        /// Starting point of the timer.
        TimerTimePoint mStart;
    protected:
    }; // class Timer
} // namespace Util

#endif //PDS_DHCP_TIMER_H
