/**
 * @file RogueMain.h
 * @author Tomas Polasek
 * @brief Main function for the dhcp rogue server application.
 */

#ifndef PDS_DHCP_ROGUEMAIN_H
#define PDS_DHCP_ROGUEMAIN_H

#include "RogueApp.h"

#endif //PDS_DHCP_ROGUEMAIN_H
