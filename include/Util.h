/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Utility functions and classes.
 */

#ifndef PDS_DHCP_UTIL_H
#define PDS_DHCP_UTIL_H

#include "Types.h"

/// Namespace for user types and utility functions.
namespace Util
{
    /**
     * Get address of given interface.
     * @id Identifier of the interface.
     * @return Return the address or throw on failure.
     * @throws std::runtime_error On failure.
     */
    in_addr intIpv4Address(const std::string &id);

    /**
     * Get address of given interface.
     * @id Identifier of the interface.
     * @return Return the address or throw on failure.
     * @throws std::runtime_error On failure.
     */
    ether_addr intMACAddress(const std::string &id);

    /**
     * Randomize given MAC address.
     * @param addr Address to start with.
     * @return Returns randomized MAC address.
     */
    ether_addr randomizeMacAddress(ether_addr addr);

    static_assert(sizeof(u_short) == sizeof(uint16_t), "They need to match");

    /**
     * Checksum calculation function by Mike Muss.
     * @param addr Address to start computing the checksum at.
     * @param len Length of the check-summed area.
     * @return Returns computed checksum.
     */
    uint16_t checksum(uint16_t *addr, std::size_t len);

    /**
     * Get index of given interface by identification.
     * @param id Identifier of the interface.
     * @return Returns index of the interface.
     * @throws std::runtime_error if the interface does not exist.
     */
    uint32_t intIndex(const std::string &id);

    /// Handler for interrupt signal. Warning: implementation is NOT thread safe!
    class SigIntHandler
    {
    public:
        /// Setup the handler for SIGINT.
        static void setup();

        /// Has the SIGINT signal been received?
        static volatile bool signalReceived();
    private:
        /// Handler called when signal is received.
        static void signalHandler(int);

        /// Flag containing information whether the SIGINT signal has been received.
        static volatile bool sReceived;
    protected:
    }; // class SigIntHandler
} // namespace Util

/// Print operator for MAC address.
std::ostream &operator<<(std::ostream &out, const ether_addr &addr);

/// Compare 2 MAC addresses.
inline bool operator==(const ether_addr &addr1, const ether_addr &addr2)
{ return std::memcmp(&addr1.ether_addr_octet, &addr2.ether_addr_octet, sizeof(addr1.ether_addr_octet)) == 0; }
inline bool operator!=(const ether_addr &addr1, const ether_addr &addr2)
{ return !(addr1 == addr2); }
inline bool operator<(const ether_addr &addr1, const ether_addr &addr2)
{
    struct CastHelper
    {
        union {
            ether_addr addr;
            uint64_t n;
        };
    };
    CastHelper first{0};
    first.addr = addr1;
    CastHelper second{0};
    second.addr = addr2;
    return first.n < second.n;
}

#endif //PDS_DHCP_UTIL_H
