# Found on : http://stackoverflow.com/questions/592620/check-if-a-program-exists-from-a-bash-script
CXX=$(shell command -v g++ || { echo >&2 "Unable to find g++."; exit 1; })
RM=rm -f
CXXFLAGS=-std=c++11 -O3 -I./include/
LDLIBS=-lpcap

SOURCE_DIR=./src

STARVE_SRC= $(SOURCE_DIR)/Util.cpp \
        $(SOURCE_DIR)/IpAddress.cpp \
        $(SOURCE_DIR)/SocketRaw.cpp \
        $(SOURCE_DIR)/SocketUdp.cpp \
        $(SOURCE_DIR)/SocketPcap.cpp \
        $(SOURCE_DIR)/Packet.cpp \
        $(SOURCE_DIR)/EthHeader.cpp \
        $(SOURCE_DIR)/IpHeader.cpp \
        $(SOURCE_DIR)/UdpHeader.cpp \
        $(SOURCE_DIR)/DhcpHeader.cpp \
        $(SOURCE_DIR)/StarveMain.cpp \
        $(SOURCE_DIR)/StarveApp.cpp
STARVE_OBJS=$(subst .cpp,.o, $(STARVE_SRC))
STARVE_DEPEND=.depend_starve
STARVE_BIN=pds-dhcpstarve

ROGUE_SRC= $(SOURCE_DIR)/Util.cpp \
        $(SOURCE_DIR)/IpAddress.cpp \
        $(SOURCE_DIR)/SocketRaw.cpp \
        $(SOURCE_DIR)/SocketUdp.cpp \
        $(SOURCE_DIR)/SocketPcap.cpp \
        $(SOURCE_DIR)/Packet.cpp \
        $(SOURCE_DIR)/EthHeader.cpp \
        $(SOURCE_DIR)/IpHeader.cpp \
        $(SOURCE_DIR)/UdpHeader.cpp \
        $(SOURCE_DIR)/DhcpHeader.cpp \
        $(SOURCE_DIR)/IpPool.cpp \
        $(SOURCE_DIR)/RogueMain.cpp \
        $(SOURCE_DIR)/RogueApp.cpp
ROGUE_OBJS=$(subst .cpp,.o, $(ROGUE_SRC))
ROGUE_DEPEND=.depend_rogue
ROGUE_BIN=pds-dhcprogue

PACK_NAME=xpolas34.zip

all: $(STARVE_BIN) $(ROGUE_BIN)

depend_starve: .depend_starve

.depend_starve: $(STARVE_SRC)
	$(RM) $(STARVE_DEPEND)
	$(CXX) $(CXXFLAGS) -MM $^>>$(STARVE_DEPEND);

include: $(STARVE_DEPEND)

$(STARVE_BIN): $(STARVE_OBJS)
	@mkdir -p bin
	$(CXX) -o ./bin/$(STARVE_BIN) $^ $(LDLIBS)

depend_rogue: .depend_rogue

.depend_rogue: $(ROGUE_SRC)
	$(RM) $(ROGUE_DEPEND)
	$(CXX) $(CXXFLAGS) -MM $^>>$(ROGUE_DEPEND);

include: $(ROGUE_DEPEND)

$(ROGUE_BIN): $(ROGUE_OBJS)
	@mkdir -p bin
	$(CXX) -o ./bin/$(ROGUE_BIN) $^ $(LDLIBS)

clean:
	$(RM) $(STARVE_OBJS) $(ROGUE_OBJS)
	$(RM) -r build
	$(RM) $(PACK_NAME)
	$(RM) bin/*

pack:
	$(RM) $(PACK_NAME)
	zip -r $(PACK_NAME) src/* include/* \
	    CMakeLists.txt Makefile dokumentace.pdf README.md

