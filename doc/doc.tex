\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{courier}
\usepackage{mathptmx}
\usepackage{sectsty}
\usepackage{changepage}
\usepackage{tikz}
\usepackage{color}
\usepackage{filecontents}
\usepackage[hidelinks]{hyperref}
\usepackage[backend=bibtex]{biblatex}
\usepackage{float}
\usepackage{listings}
\usepackage{tabularx}
\usepackage[czech]{babel}

\renewcommand{\figurename}{Obr.}
\renewcommand{\contentsname}{Obsah}

\graphicspath{{./images/}}

\newcommand{\todo}[1]{\textcolor{red}{\textbf{[[#1]]}}}

%\begin{filecontents}{doc.bib}
%\end{filecontents}
\addbibresource{doc.bib}

\title{DHCP Útoky}
\author{Tomáš Polášek (xpolas34)}
\date{\today}

\begin{document}

\begin{titlepage}
\huge
\noindent
\center{\textsc{\LARGE VYSOKÉ UČENÍ TECHNICKÉ V BRNĚ}}
\center{\textsc{\Large FAKULTA INFORMAČNÍCH TECHNOLOGIÍ}}
\newline
\begin{figure}[h]
\begin{center}
  \scalebox{0.4}
  {
    \includegraphics[scale=0.3]{FIT_logo}
  }
  \label{pic:fit_logo}
\end{center}
\end{figure}

\center{Projekt do předmětu PDS}
\center{MIT1 2017/2018}
% Make title without page break.
{\let\newpage\relax\maketitle}
% Reset the page style
\thispagestyle{empty}

\end{titlepage}

\tableofcontents

\pagebreak

\section{Úvod}

Tento dokument obsahuje popis řešení projektu zabývajícím se útoky na \textit{Dynamic Host Configuration Protocol} (\textit{DHCP}). Byl vytvořen jako součást hodnocení do předmětu \uv{Přenos dat, počítačové sítě a protokoly} na VUT FIT. 

První kapitola obsahuje rozbor funkcionality \textit{DHCP} a možnosti útoků na něj. Následuje popis zajímavých částí implementací, které popsané útoky realizují. Dále dokumentace obsahuje demonstrace činnosti těchto aplikací na testovacím scénáři. Na závěr je celý projekt krátce zhodnocen.

\pagebreak

\section{Teoretický rozbor}

Tato kapitola se krátce zabývá důvodem existence a samotnou funkcí \textit{DHCP}. Následně jsou nastíněny typy útoků, které jsou dále implementovány v testovacích aplikacích.

\subsection{Funkce DHCP}

\textit{DHCP} (\textit{Dynamic Host Configuration Protocol}) je aplikačním protokolem využívaným pro dynamickou konfiguraci síťových zařízení, které se připojují k síti. Samotný protokol operuje na L7 ISO/OSI modelu a používá architekturu klient-server.

Komunikace začíná ve většině případu tím, že klient zašle DHCP zprávu typu \textit{DISCOVER}, která je mířena na obecnou \textit{broadcast} adresu (255.255.255.255). Grafickou reprezentaci této komunikace lze vidět na obr. \ref{Obr:DhcpCommunication}.

Následně DHCP servery, které mají zájem poskytnout konfigurační informace novému klientovi zašlou, opět na \textit{broadcast} adresu, zprávu typu \textit{OFFER}. V této správě klientovi zasílá nabídku IP adresy a další informace, mezi které patří maska sítě, \textit{domain name server}, \textit{gateway}, dobu výpůjčky, doménu a další. 

Klient si dále může vybrat jednu z nabídek, pokud nějaká přišla a odpovídá na ni zprávou typu \textit{REQUEST}. Tato zpráva, opět směřována na všechna zařízení (\textit{broadcast}), navíc ale obsahuje specifikaci serveru, kterému klient odpovídá. Díky tomu daný DHCP server pozná, že klient odpovídá jemu. Pokud požadavek od klienta je validní, server odpovídá konečnou zprávou typu \textit{ACKNOWLEDGE}.

Nutno dodat, že výše popsaný popis komunikace neobsahuje celý rozsah zpráv, které jsou v DHCP dostupné. Příkladem je například zpráva typu \textit{NACK}, kterou může DHCP server zasílat v případě, že požadavek klienta není korektní. Celý rozsah zpráv lze vidět např v RFC2131 \cite{Dhcp} na straně 14.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{DHCP_com}
	\caption{Komunikace klienta a serveru za pomocí DHCP \cite{DhcpCommunication}.}
	\label{Obr:DhcpCommunication}
\end{figure}

\subsection{Hlavička DHCP}

Hlavička DHCP obsahuje mnoho informací \cite{DhcpFormat}, přičemž tato část krátce popisuje pouze ty, které jsou důležité pro následnou implementaci útoků. Grafickou reprezentaci hlavičky lze vidět na obr. \ref{Obr:DhcpHeader}. DHCP hlavička je specifikací hlavičky protokolu \textit{BOOTP}.

První z těchto polí je samotný typ zprávy -- v tomto případě pouze zda jde o požadavek nebo o odpověď. Další zajímavou hodnotou je až \textit{XID}, která by měla jednoznačně určovat komunikaci mezi klientem a serverem. V našem případě bude toto pole použito pro identifikaci zpráv, které bude generovat nástroj pro zahlcení skutečných DHCP serverů. Díky tomu, že tento identifikátor pevně nastavíme můžeme zprávy v \uv{rogue} DHCP serveru filtrovat tak, abychom nezahltili sami sebe. 

Dále hlavička obsahuje pole příznaků, kde pouze jeden z nich je definován ve standardu \cite{DhcpFormat}. Tento příznak specifikuje, že server má zasílat zprávy na broadcast adresu a je použit i v našem případě. 

Následují tři adresy, jejichž význam je následující. První z nich je vyplněna klientem, který již má IP adresu a chce ji obnovit. Druhá i třetí jsou nastaveny DHCP serverem ve zprávě typu \textit{OFFER} a značí nabízenou IP adresu a IP adresu DHCP serveru. 

Dále hlavička obsahuje \uv{hardware} adresu -- v našem případě MAC adresu -- klienta. Tato adresa je používána DHCP serverem pro mapování IP adres zařízením a je důvodem, proč aplikace zahlcení funguje.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{DHCP_header}
	\caption{Hlavička DHCP zprávy \cite{Dhcp}.}
	\label{Obr:DhcpHeader}
\end{figure}

Poslední částí DHCP hlavičky je pole volitelných možností, které je používáno pro specifikaci všech možností \cite{DhcpOptions}, které neobsahuje samotná hlavička \textit{BOOTP}. Jednotlivé možnosti jsou zapsány jedním bytem s identifikátorem možnosti, jedním bytem délkou dat možnosti a danými daty. Toto pole je následně uzavřeno bytem s hodnotou \textit{0xFF}.

Nejdůležitější volitelnou možností je samotná specifikace typu zprávy DHCP -- např. \textit{DISCOVER}, \textit{OFFER}, \textit{REQUEST} nebo \textit{ACK}. Mezi další, často používané, možnosti patří \cite{DhcpCommunication} vyžadovaná IP adresa, IP adresa DHCP serveru, seznam požadovaných informací a všechny volitelně odesílané informace klientovi -- maska, gateway atp.

\subsection{Metody útoků}

Tato část obsahuje dva typy často používaných útoků na DHCP servery v \textit{IPv4} sítích. Prvním z nich je útok \uv{vyhladovění}, který lze považovat za útok typu \textit{DOS} (\textit{Denial Of Service}). Cílem takového útoku je vyčerpat zásobu IP adres, které DHCP server v dané síti operuje, čímž znemožní připojování dalších dynamicky konfigurovaných zařízení.

Hlavní myšlenkou tohoto útoku (obr. \ref{Obr:DhcpStarvation}) \cite{DhcpStarvation} je taková, že zařízení útočníka simuluje existenci dalších zařízení, které požadují IP adresu od DHCP serveru. Toto je možné, jelikož je každé nové zařízení v síti, před tím, než je mu přidělena IP adresa identifikováno pouze svojí MAC adresou. Útočník tedy může generovat novou MAC adresu pro každé své nové \uv{imaginární} zařízení. Následně pro každé zařízení generuje zprávy typu \textit{DISCOVER}, přičemž do pole \textit{chaddr} dosadí nově vygenerovanou MAC adresu. 

Po přijetí zprávy \textit{DISCOVER} a odpovězení pomocí \textit{OFFER} by, podle RFC2131 \cite{Dhcp} str. 27, DHCP server \textbf{neměl} nabídnutou adresu nabízet dalším klientům. Díky tomu by teoreticky mělo útočníkovi stačit zasílat zprávy \textit{DISCOVER} dokud mu přicházejí nabídky od DHCP serverů. Bezpečnější varianta je však projít celou komunikací (\textit{REQUEST} a \textit{ACKNOWLEDGE}), čímž je IP adresa zaručeně zarezervována \uv{virtuálním} klientem na plnou dobu výpůjčky.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{DHCP_starvation}
	\caption{Schéma útoku typu \uv{vyhladovění} \cite{DhcpStarvation}.}
	\label{Obr:DhcpStarvation}
\end{figure}

Dalším typem útoku je zavedení záškodnického DHCP serveru (obr. \ref{Obr:DhcpRogue}) \cite{DhcpRogue}, který podvrhuje odpovědi reálného DHCP serveru. Útočník v tomto případě spustí vlastni DHCP server, který odpovídá potenciálním klientům falešnými informacemi, díky čemuž lze např. realizovat útoky typu \textit{DOS} (\textit{Denial Of Service}) a \textit{MITM} (\textit{Man in the Middle}). Problémem u tohoto útoku je, že záškodnický server se spoléhá na to, že dokáže potenciálnímu klientovi odpovědět rychleji, než server skutečný.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{DHCP_rogue}
	\caption{Schéma útoku záškodnického DHCP serveru \cite{DhcpRogue}, který je kombinován s útokem \uv{vyhladovění} \cite{DhcpStarvation}.}
	\label{Obr:DhcpRogue}
\end{figure}

Výše zmíněné útoky lze také kombinovat, čímž lze zvýšit jejich efektivitu \cite{DhcpRogue}. Jedním způsobem je nejdříve vyčerpat zásoby IP adres pomocí \textit{DHCP starvation} útoku a následně teprve spustit vlastní záškodnický DHCP server. Tímto, alespoň na určitou dobu, odpadá problém se soupeřením skutečných DHCP serverů s útočníkovým.

Existuje mnoho možností, jak se proti těmto útokům bránit. Mezi nejčastější patří tzv. \textit{DHCP snooping} \cite{DhcpSnooping}, který umožňuje např. nastavit \uv{důvěryhodné} porty, na kterých běží skutečné DHCP servery a odpovědi z ostatních zahazovat. Ochranou proti generování \uv{virtuálních} zařízení je např. omezení počtu MAC adres na jednom portu \cite{StarvationLimit}.

\section{Implementace}

Tato kapitola obsahuje zajímavosti a postřehy, které vznikly při implementaci aplikací umožňujících útoky typu \textit{DHCP starvation} a \textit{DHCP rogue server}.

\subsection{Vyhladovění}

Úkolem první aplikace je implementace útoku typu \uv{vyhladovění}. Vstupem aplikace je rozhraní, skrz které má vyhledávat cílové DHCP servery, které následně zahltí požadavky a vyčerpá jejich zásobu IP adres. Navíc aplikace umožňuje specifikovat \textit{XID}, kterými budou identifikovány její DHCP zprávy. Této možnosti lze využít pro synchronizaci obou aplikací.

Kvůli požadavku, aby DHCP klient vytvářel pakety se zdrojovou IP adresou \textbf{0.0.0.0} je bylo při implementaci nutné použit \textit{sockety} v módu \textit{RAW} \cite{RawSock}. Z důvodu přenositelnosti bylo místo standardních \textit{UNIX socketů} použita knihovna \textit{PCAP} \cite{Pcap}, která umožňuje zachytávání paketů a jejich filtraci pomocí \textit{BPF}(\textit{Berkeley Packet Filter}). Navíc, aby bylo možné skutečně nastavit IP adresu na požadovanou \textbf{0.0.0.0}, je nutné zprávy stavět od \textit{Ethernetové} hlavičky nahoru.

Po spuštění si aplikace vytvoří jednotlivé hlavičky -- \textit{Ethernetovou} \cite{EthernetHeader}, \textit{IPv4} \cite{Ipv4Header}, \textit{UDP} \cite{UdpHeader} a na závěr samotnou \textit{DHCP} hlavičku \cite{DhcpFormat}. Zajímavou částí je zdrojová \textit{MAC} adresa, která je nastavena podle \textit{MAC} adresy cílového rozhraní. Tímto chování zařízení, které vidí síťový prvek do kterého je útočník připojen, vypadá, jako kdyby bylo switch/hub a připojovalo do sítě další zařízení.

Dále aplikace prochází následující akce, do té doby, než přestanou DHCP servery odpovídat -- čeká, až budou vyčerpány:

\begin{enumerate}
	\item Pokud byl vyčerpán počet pokusů, aniž by DHCP server odpověděl, aplikace končí. Jinak vyšle zprávu typu \textit{DISCOVER}.
    \item Vyčkává na zprávu typu \textit{OFFER}. Pokud tato zpráva nedojde, potom se vrací se do kroku \textbf{(1)}.
    \item Zašle zprávu typu \textit{REQUEST} s informacemi, které mu byly poskytnuty v kroku \textbf{(2)}.
    \item Vyčkává na zprávu typu \textit{ACKNOWLEDGE}. Pokud nepřijde do určitého počtu čekání, potom se vrací do kroku \textbf{(1)}. Jinak vygeneruje novou MAC adresu a vrací se do kroku \textbf{(1)}.
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{App_starve}
	\caption{Návod k použití aplikace pro \uv{vyhladovění} DHCP serveru.}
	\label{Obr:AppStarve}
\end{figure}

\pagebreak

\subsection{DHCP server}

Úkolem druhé aplikace je implementace jednoduchého DHCP serveru, který lze použít pro útok typu \uv{záškodnický server}. Aplikace tedy simuluje funkci reálného DHCP serveru a odpovídá na \textit{DISCOVER} zprávy (obr. \ref{Obr:DhcpCommunication}). 

Samotná implementace používá podobné stavební prvky, jako aplikace první. Pro práci se sockety je použiva knihovna \textit{PCAP} \cite{Pcap}. Ze začátku si aplikace vytvoří všechny potřebné hlavičky jednotlivých protokolů -- \textit{Ethernetovou} \cite{EthernetHeader}, \textit{IPv4} \cite{Ipv4Header}, \textit{UDP} \cite{UdpHeader} a \textit{DHCP} \cite{DhcpFormat}. Následuje smyčka, kdy server čeká na požadavky klientů: 

\begin{enumerate}
	\item Server čeká na zprávy od klientů. Pokud přijme zprávu typu \textit{DISCOVER}, přechází do kroku \textbf{(2)}. Pokud přijme zprávu typu \textit{REQUEST}, přejde do kroku \textbf{(3)}. Jinak zůstává v kroku \textbf{(1)}.
    \item Vygeneruje novou IP adresu z \textit{IP poolu}. Pokud již žádná IP adresa není volná, požadavek ignoruje a přechází zpět do kroku \textbf{(1)}. Jinak zašle klientovi zprávu \textit{OFFER} s vygenerovanou IP adresou. Na závěr přechází do stavu \textbf{(1)}.
    \item Požadovaná IP adresa může být v několika stavech, na jehož základě se server rozhoduje. Pokud je volná potom ji namapuje na klientovu MAC adresu a zašle mu potvrzení \textit{ACKNOWLEDGE}. Podobně se zachová, když IP adresa volná není, ale je již mapována na klientovu MAC adresu. V tomto případě pouze nastaví aktuální \textit{lease time}. Poslední možností je případ, kdy je IP adresa již pronajatá jinému klientovu, v takovém případě server odesílá zprávu \textit{NACK}. Na závěr opět přechází zpět do stavu \textbf{(1)}.
\end{enumerate}

Zajímavou částí byla implementace samotného \textit{IP poolu}, jehož funkcí je evidence vydaných IP adres. Kromě samotného seznamu použitých IP adres obsahuje také informaci o čase, kdy byla IP adresa vypůjčena a po vyčerpání \textit{lease time} je opět uvedena do oběhu. Další funkcí je mapování \textit{IP adresa} $\leftrightarrow$ \textit{záznam vypůjčení} $\leftrightarrow$ \textit{MAC adresa}, což umožňuje rychlou detekci, zda klient již vlastní/vlastnil požadovanou IP adresu.

Další zajímavostí je parametr \textit{XID}, který umožňuje synchronizaci aplikace \uv{vyhladovění} a záškodnického DHCP serveru. Server kontroluje \textit{XID} a pokud je stejný, jako ten specifikovaný daným parametrem, potom zprávy ignoruje. Toto umožňuje spustit obě aplikace zároveň.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{App_rogue}
	\caption{Návod k použití aplikace pro \textit{rogue DHCP server}.}
	\label{Obr:AppRogue}
\end{figure}

\pagebreak

\section{Demonstrace činnosti}

Tato kapitola obsahuje testovací scénář, na kterém byly výše popsané aplikace testovány. Samotná laboratoř využívá virtualizovanou topologii, jejíž schéma lze vidět na obr. \ref{Obr:Network}. Virtualizace byla uskutečněna za pomocí nástroje \textit{Virtualbox} \cite{Virtualbox}. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Network}
	\caption{Testovací síťová topologie realizovaná pomocí virtualizačního nástroje.}
	\label{Obr:Network}
\end{figure}

Všechna virtuální zařízení byla připojena pomocí možností \textit{Internal Network}. DHCP server i útočník oba používají OS \textit{Ubuntu 14.04.3 LTS}. Klienti používají živý obraz distribuce \textit{Arch Linux}. 

Prvním krokem je inicializace stavu sítě. Po nastartování všech čtyř virtuálních strojů je třeba provést počáteční konfiguraci: 

\begin{itemize}
	\item \textbf{Klienti}: 
    \begin{itemize}
    	\item Pro pozdější simulaci připojení klienta k síti byly oběma klientům vypnuty rozhraní, kterými jsou připojeni k síti. Tohoto lze dosáhnout příkazem: \texttt{sudo ip link set enp0s3 down}.
    \end{itemize}
	\item \textbf{Útočník}: 
    \begin{itemize}
    	\item Podobně bylo vypnuto rozhraní i útočníkovi: \\ \texttt{sudo ip link set eth0 down}.
    \end{itemize}
	\item \textbf{DHCP server}: 
    \begin{itemize}
    	\item Rozhraní \textbf{eth0} bylo staticky nastaveno podle schématu topologie.
        \item Byla provedena konfigurace DHCP serveru (\texttt{udhcpd}) tak, aby: 
        \begin{itemize}
        	\item Vydal maximálně 10 IP adres z rozsahu \textit{192.168.0.20} - \textit{192.168.0.40}. Omezené množství bylo nastaveno z důvodu času, který zabere zásobu vyčerpat následným útokem.
            \item Aby poskytoval klientům gateway (\textit{192.168.0.1}), DNS (\textit{8.8.8.8}) a masku (\textit{255.255.255.0}).
        \end{itemize}
        \item Nakonec byl DHCP server uveden do chodu příkazem: \\ \texttt{sudo udhcpd -f}.
    \end{itemize}
\end{itemize}

Dále bylo simulování připojení prvního klienta k síti, rozhraní \textbf{enp0s3} bylo opět spuštěno a pomocí příkazu \texttt{dhclient} byl vyslán požadavek DHCP serveru. V tomto případě přidělil DHCP server IP adresu \textit{192.168.0.25} (obr. \ref{Obr:FirstClient}).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Lab_FirstClient}
	\includegraphics[width=0.6\textwidth]{Lab_FirstClient_lease}
	\caption{Konfigurace prvního klienta, která byla získána ze skutečného DHCP serveru.}
	\label{Obr:FirstClient}
\end{figure}

V dalším kroku bylo podobně simulováno útočníkova počítače, který získal IP adresu \textit{192.168.0.29}. Následně útočník spouští výše popsané aplikace: 
\begin{lstlisting}
        sudo ./pds-dhcpstarve -i eth0 -x 1337
        sudo ./pds-dhcprogue -i eth0 \
          -p 192.168.0.50-192.168.0.60 \
          -g 192.168.0.29 -n 192.168.0.29 \
          -d fit.vutbr.cz -l 3600 -x 1337
\end{lstlisting}

Po chvíli lze vidět, že aplikace \uv{vyhladovění} splnila svůj cíl a původní DHCP server již nemá volné IP adresy. Výstupy lze vidět na obr. \ref{Obr:StarvationApp}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Lab_DhcpStarvation_App}
	\includegraphics[width=0.6\textwidth]{Lab_DhcpStarvation}
	\caption{Výstup aplikace pro \uv{vyhladovění} DHCP serveru a následná reakce cíleného DHCP serveru.}
	\label{Obr:StarvationApp}
\end{figure}

Posledním krokem laboratoře je simulace připojení dalšího klienta k síti. Toto nastane nějakou dobu po vyčerpání zásoby IP adres originálního DHCP serveru, aby bylo jisté, že požadavek zachytí záškodnický server. Výsledný výstup komunikace mezi útočníkovým DHCP serverem a novým klientem lze vidět na obr. \ref{Obr:StarvationApp}. V informacích o klientově výpůjčce (obr. \ref{Obr:SecondClient}) lze vidět, že klient skutečně dostal podvržené informace, které byly specifikovány.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{Lab_DhcpRogue_App}
	\caption{Výstup záškodnického DHCP serveru.}
	\label{Obr:StarvationApp}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{Lab_SecondClient_lease}
	\caption{Výpůjčka druhého klienta, kterému byly zaslány falešné informace.}
	\label{Obr:SecondClient}
\end{figure}

\pagebreak

% Rename the reference chapter to "Reference"
\renewcommand{\refname}{Reference}
\printbibliography

\end{document}
