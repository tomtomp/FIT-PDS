What
====

This is a project for the "Data Communications, Computer Networks and Protocols" course on the BUT FIT. Its main goal is to analyze and create example applications that realize DHCP starvation and DHCP rogue server attacks.

Content
=======

The project contains following files: 
 * **src/** - Contains source codes in C++
 * **include/** - Contains include files.
 * **doc/** - Documentation source in LaTeX.
 * **CMakeLists.txt** - CMake script used for building of the applications.
 * **Makefile** - Makefile used for building of the applications.
 * **dokumentace.pdf** - Project documentation in Czech language.

Building
========

Requirements
------------

Following software is required for building of the example applications: 
 * C++ compiler which supports **C++11** standard - Tested on GCC 4.8.4 .
 * PCAP library installed on the system.

 **NOTE: Applications were only tested on Unix based systems!**

Build
-----

Building of the example applications can be performed by using included **Makefile**: 
```
    cd <root_of_repo>/
    make
```
or **CMakeLists** script: 
```
    cd <root_of_repo/
    mkdir build && cd build
    cmake ..
```

In Either case the output will be stored in the **bin** directory.

Usage
=====

This repository contains two example applications, first is the **pds-dhcpstarve** which allows the attacker to starve out a target DHCP server. This can be used for DOS attack on target network. Its usage is following: 
```
    Usage: ./pds-dhcpstarve -i interface [-x xid]           
      interface: Interface name used in the attack.   
      xid: Optional parameter for setting message XID.
```

The second application is a simplified DHCP server implementation which has additional feature which allows synchronization with the **pds-dhcprogue** application. It can be used in following way: 
```
    Usage: ./pds-dhcprogue -i interface -p pool -g gateway -n dns-server -d domain -l lease-time [-x xid]
      interface: Interface name used in the attack.
      pool: Address pool used by the server. Specified in format <first_ip>-<last_ip.
      gateway: Gateway address supplied to the clients.
      dns-server: DNS server address supplied to the clients.
      domain: Domain name supplied to the clients.
      lease-time: How long in seconds does a lease hold.
      xid: Optional parameter for setting message XID used by the starve application.
```

