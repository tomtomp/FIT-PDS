/**
 * @file IpHeader.cpp
 * @author Tomas Polasek
 * @brief Ip header abstraction.
 */

#include "IpHeader.h"

namespace Util
{
    IpHeader::IpHeader()
    {
        initialize();
    }

    void IpHeader::initialize()
    {
        zero();
        mIpHdr.ihl = HDR_IHL;
        mIpHdr.version = HDR_VERSION;
        mIpHdr.tos = HDR_TOS;
        mIpHdr.tot_len = HDR_TOTAL_LENGTH;
        mIpHdr.id = htons(HDR_ID);
        mIpHdr.frag_off = HDR_FRAG_OFFSET;
        mIpHdr.ttl = HDR_TTL;
        mIpHdr.protocol = HDR_PROTO;
        mIpHdr.check = HDR_CHECK;
        mIpHdr.saddr = HDR_ADDR;
        mIpHdr.daddr = HDR_ADDR;

        mDirty = true;
    }

    void IpHeader::recalcChecksum()
    {
        if (mDirty)
        {
            // Set checksum to zero.
            mIpHdr.check = HDR_CHECK;
            // Reinterpret_cast should be OK, since the iphdr structure contains uint16_t's.
            mIpHdr.check = Util::checksum(reinterpret_cast<uint16_t*>(rawData()), size());
            mDirty = false;
        }
    }

    void IpHeader::zero()
    {
        std::memset(rawData(), 0, size());
    }

    const uint8_t *IpHeader::data()
    {
        if (mDirty)
        {
            recalcChecksum();
        }

        return reinterpret_cast<uint8_t*>(&mIpHdr);
    }

    const uint8_t *IpHeader::data() const
    {
        if (mDirty)
        {
            throw std::runtime_error("Unable to recalculate IP header checksum on const object!");
        }

        return reinterpret_cast<const uint8_t*>(&mIpHdr);
    }

    void IpHeader::setPayloadLength(uint16_t len)
    {
        mIpHdr.tot_len = htons(size() + len);
        mDirty = true;
    }

    void IpHeader::setId(uint16_t id)
    {
        mIpHdr.id = id;
        mDirty = true;
    }

    void IpHeader::setProto(uint8_t proto)
    {
        mIpHdr.protocol = proto;
        mDirty = true;
    }

    void IpHeader::setSource(const Ipv4Address &src)
    {
        mIpHdr.saddr = src.getAddr();
        mDirty = true;
    }

    void IpHeader::setDst(const Ipv4Address &dst)
    {
        mIpHdr.daddr = dst.getAddr();
        mDirty = true;
    }
}
