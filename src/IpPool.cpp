/**
 * @file IpPool.cpp
 * @author Tomas Polasek
 * @brief Pool of IP addresses.
 */

#include "IpPool.h"

namespace Util
{

    IpPool::IpPool(const std::string &range)
    {
        initialize(range);
    }

    IpPool::IpPool(const Util::Ipv4Address &first, const Util::Ipv4Address &last)
    {
        initialize(first, last);
    }

    void IpPool::initialize(const std::string &range)
    {
        // Find the separator
        auto dash = range.find('-');
        if (dash == range.npos)
        {
            throw std::runtime_error("Unable to find dash in provided pool specification string!");
        }

        // Get the two parts.
        std::string first{range.substr(0, dash)};
        std::string second{range.substr(dash + 1u)};

        in_addr res;

        // Convert the first IP address.
        if(!inet_aton(first.c_str(), &res))
        {
            throw std::runtime_error("Unable to convert first IP address in pool specification! : " + first);
        }
        uint32_t firstIp{res.s_addr};

        // Convert the second IP address.
        if(!inet_aton(second.c_str(), &res))
        {
            throw std::runtime_error("Unable to convert second IP address in pool specification! : " + second);
        }
        uint32_t lastIp{res.s_addr};

        initialize(firstIp, lastIp);
    }

    void IpPool::initialize(const Util::Ipv4Address &first, const Util::Ipv4Address &last)
    {
        initialize(first.getAddr(), last.getAddr());
    }

    void IpPool::initialize(uint32_t first, uint32_t last)
    {
        mFirstIp = ntohl(first);
        mLastIp = ntohl(last);
        if (mFirstIp > mLastIp)
        {
            throw std::runtime_error("Ip pool needs to have at least one IP address");
        }
        reset();
    }

    void IpPool::reset()
    {
        mLeaseTime = DEFAULT_LEASE_TIME;
        mLeasesIp.clear();
        mLeasesMac.clear();
        // Minus one to prepare for the nextIp call which increments the current IP address.
        mCurrentIp = mFirstIp - 1u;
    }

    void IpPool::setLeaseTime(std::size_t leaseTime)
    {
        mLeaseTime = leaseTime;
    }

    Util::Ipv4Address IpPool::leaseIp(const ether_addr &mac)
    {
        // Check if the MAC address already has an IP address leased.
        auto macRec = mLeasesMac.find(mac);
        if (macRec != mLeasesMac.end())
        { // Found a record for the MAC address.
            // Check if the IP address still belongs to given device.
            auto macIpRec = mLeasesIp.find(macRec->second.ip);
            if (macIpRec != mLeasesIp.end())
            { // Found the IP record.
                if (macIpRec->second.mac == mac)
                { // It does/did belong to given device.
                    macIpRec->second.renewRecord();
                    return Util::Ipv4Address(htonl(macIpRec->first));
                }
            }
        }

        // Else find a new IP address for it.

        // Move to the next available IP address.
        nextIp();

        // Set the records.
        uint32_t newIp = mCurrentIp;
        innerLeaseIp(mac, newIp);

        return Util::Ipv4Address(htonl(newIp));
    }

    Util::Ipv4Address IpPool::leaseIp(const ether_addr &mac, const Util::Ipv4Address &preferred)
    {
        uint32_t preferredIp{ntohl(preferred.getAddr())};
        // Check if the IP address is free.
        if (preferredIp >= mFirstIp && preferredIp <= mLastIp && ipFree(preferredIp))
        { // The IP address is free for use.
            innerLeaseIp(mac, preferredIp);
            // Important to move the current IP since it won't be free anymore!
            if (mCurrentIp == preferredIp)
            {
                nextIp();
            }
            return preferred;
        }
        // Else fall back to normal method.
        return leaseIp(mac);
    }

    void IpPool::renewIp(const Util::Ipv4Address &ip)
    {
        uint32_t ipAddr{ntohl(ip.getAddr())};
        auto ipRec = mLeasesIp.find(ipAddr);
        if (ipRec != mLeasesIp.end())
        {
            ipRec->second.renewRecord();
        }
    }

    bool IpPool::ipBelongsTo(const Util::Ipv4Address &ip, const ether_addr &mac) const
    {
        uint32_t ipAddr{ntohl(ip.getAddr())};

        auto macIpRec = mLeasesIp.find(ipAddr);
        if (macIpRec != mLeasesIp.end())
        { // Found the IP record.
            return (macIpRec->second.mac == mac && macIpRec->second.leasedFor() < mLeaseTime);
        }

        return false;
    }

    void IpPool::releaseIp(const Util::Ipv4Address &ip)
    {
        uint32_t ipAddr{ntohl(ip.getAddr())};
        auto ipRec = mLeasesIp.find(ipAddr);
        if (ipRec != mLeasesIp.end())
        {
            mLeasesMac.erase(ipRec->second.mac);
            mLeasesIp.erase(ipRec);
        }
    }

    void IpPool::printLeased(bool printExpired) const
    {
        auto now = LeaseClock::now();

        std::cout << "IpPool : " << Util::Ipv4Address(htonl(mFirstIp))
                  << " - " << Util::Ipv4Address(htonl(mLastIp))
                  << " : ";
        for (auto &rec : mLeasesIp)
        {
            if (printExpired || rec.second.leasedFor() < mLeaseTime)
            {
                std::cout << "\n\t" << Util::Ipv4Address(htonl(rec.first)) << " : " << rec.second.mac << " at "
                          << LeaseClock::to_time_t(rec.second.leaseTime) << " ("
                          << (std::chrono::duration_cast<std::chrono::milliseconds>(now - rec.second.leaseTime).count()
                              / static_cast<float>(LeaseRecord::MS_IN_S)) << " s)";
            }
        }
        std::cout << std::endl;
    }

    void IpPool::nextIp()
    {
        // Start by moving over one.
        uint32_t newCurrentIp{mCurrentIp + 1u};
        if (newCurrentIp > mLastIp)
        {
            newCurrentIp = mFirstIp;
        }

        // Test for wrap-around and search for a free IP address.
        while (newCurrentIp != mCurrentIp && !ipFree(newCurrentIp))
        {
            if (newCurrentIp > mLastIp)
            {
                newCurrentIp = mFirstIp;
            }
            else
            {
                newCurrentIp++;
            }
        }

        if (newCurrentIp == mCurrentIp)
        { // Wrapped around, no free IP addresses!
            throw std::runtime_error("No more free IP addresses available!");
        }

        // We found the new free IP address.
        mCurrentIp = newCurrentIp;
    }

    void IpPool::innerLeaseIp(const ether_addr &mac, uint32_t ip)
    {
        auto &ipRec = mLeasesIp[ip];
        auto &newMacRec = mLeasesMac[mac];

        ipRec.mac = mac;
        ipRec.renewRecord();
        newMacRec.ip = ip;
    }

    bool IpPool::ipFree(uint32_t ip) const
    {
        if (ip < mFirstIp || ip > mLastIp)
        {
            return false;
        }
        auto ipRec = mLeasesIp.find(ip);
        return (ipRec == mLeasesIp.end() || ipRec->second.leasedFor() >= mLeaseTime);
    }
}
