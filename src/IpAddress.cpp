/**
 * @file IpAddress.cpp
 * @author Tomas Polasek
 * @brief IP address abstraction.
 */

#include "IpAddress.h"

namespace Util
{
    Util::Ipv4Address::Ipv4Address()
    {
        reset();
    }

    Ipv4Address::Ipv4Address(const std::string &addr)
    {
        setFrom(addr);
    }

    Ipv4Address::Ipv4Address(const in_addr &addr)
    {
        setFrom(addr);
    }

    Ipv4Address::Ipv4Address(uint32_t addr)
    {
        setFrom(addr);
    }

    Ipv4Address::Ipv4Address(uint8_t o3, uint8_t o2, uint8_t o1, uint8_t o0)
    {
        setFrom(o3, o2, o1, o0);
    }

    void Ipv4Address::setFrom(const std::string &addr)
    {
        if (!inet_aton(addr.c_str(), &mAddress))
        {
            throw std::runtime_error("Unable to parse IPv4 address! : " + addr);
        }
    }

    void Ipv4Address::setFrom(const in_addr &addr)
    {
        mAddress = addr;
    }

    void Ipv4Address::setFrom(uint32_t addr)
    {
        mAddress.s_addr = addr;
    }

    void Ipv4Address::setFrom(uint8_t o3, uint8_t o2, uint8_t o1, uint8_t o0)
    {
        mAddress.s_addr = static_cast<uint16_t>(o0) << (3 * BITS_IN_BYTE)
                          | static_cast<uint16_t>(o1) << (2 * BITS_IN_BYTE)
                          | static_cast<uint16_t>(o2) << (1 * BITS_IN_BYTE)
                          | static_cast<uint16_t>(o3);
    }

    void Ipv4Address::reset()
    {
        std::memset(address(), 0, addressSize());
    }

    void Ipv4Address::print(std::ostream &stream) const
    {
        stream << "IPv4 : " << getOctet(0) << "." << getOctet(1) << "." << getOctet(2) << "." << getOctet(3);
    }

    std::ostream &operator<<(std::ostream &stream, const Ipv4Address &address)
    {
        address.print(stream);
        return stream;
    }
}
