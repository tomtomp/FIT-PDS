/**
 * @file DhcpPacket.cpp
 * @author Tomas Polasek
 * @brief DHCP packet abstraction. Used for creation and parsing of DHCP packets.
 */

#include "DhcpHeader.h"

namespace Util
{
    DhcpHeader::DhcpHeader(const std::vector<uint8_t> &data, std::size_t startIdx)
    {
        loadFrom(data, startIdx);
    }

    void DhcpHeader::loadFrom(const std::vector<uint8_t> &data, std::size_t startIdx)
    {
        copyData(data, startIdx);
        parseMessage();
    }

    DhcpHeader::DhcpHeader()
    {
        reset();
    }

    void DhcpHeader::reset()
    {
        std::memset(&mMessage, 0, size());

        mUsedOptionBytes = 0;

        mMessage.op = PktData::REQUEST;
        mParsed.type = DhcpMessageType::Request;
        mMessage.htype = PktData::HTYPE_ETHERNET;
        mParsed.hType = DhcpHardwareType::Ethernet;
        mMessage.hlen = PktData::HLEN_ETHERNET;
        mParsed.hAddrLen = PktData::HLEN_ETHERNET;
        mMessage.hops = 0;
        mParsed.hops = 0;

        mMessage.xid = htonl(0x1EE7);
        mParsed.xid = 0x1EE7;
        mMessage.secs = 0;
        mParsed.secs = 0;
        mMessage.flags = PktData::FLAG_BROADCAST;
        mParsed.broadcastFlag = true;
        //mMessage.flags = 0u;
        //mParsed.broadcastFlag = false;

        /*
         * Left zeroed:
         *   mMessage.ciAddr
         *   mMessage.yiAddr
         *   mMessage.siAddr
         *   mMessage.giAddr
         *   mMessage.chAddr
         *   mMessage.sName
         *   mMessage.fileName
         *   mMessage.options
         */
        std::memset(mParsed.macAddr.ether_addr_octet, 0, ETH_ALEN);

        mMessage.options[mUsedOptionBytes++] = PktData::MAGIC_COOKIE[0];
        mMessage.options[mUsedOptionBytes++] = PktData::MAGIC_COOKIE[1];
        mMessage.options[mUsedOptionBytes++] = PktData::MAGIC_COOKIE[2];
        mMessage.options[mUsedOptionBytes++] = PktData::MAGIC_COOKIE[3];

        pushOptMsgType(PktData::DHCPDISCOVER);
    }

    void DhcpHeader::copyData(const std::vector<uint8_t> &data, std::size_t startIdx)
    {
        if ((data.size() - startIdx) > size())
        {
            throw std::runtime_error("Data vector passed to DhcpHeader too large! : " + std::to_string(data.size() - startIdx));
        }
        if (startIdx > data.size())
        {
            throw std::runtime_error("Starting index cannot be further than the size of the message");
        }

        std::memcpy(&mMessage, data.data() + startIdx, data.size() - startIdx);
    }

    void DhcpHeader::parseMessage()
    {
        switch (mMessage.op)
        {
            case PktData::REQUEST :
            {
                mParsed.type = DhcpMessageType::Request;
                break;
            }
            case PktData::REPLY :
            {
                mParsed.type = DhcpMessageType::Reply;
                break;
            }
            default:
            {
                throw std::runtime_error("Unknown DHCP message OP code! : " + std::to_string(mMessage.op));
            }
        }

        if (mMessage.htype != PktData::HTYPE_ETHERNET)
        {
            throw std::runtime_error("Unknown DHCP HType! : " + std::to_string(mMessage.htype));
        }
        mParsed.hType = DhcpHardwareType::Ethernet;

        if (mMessage.hlen != PktData::HLEN_ETHERNET)
        {
            throw std::runtime_error("Unknown DHCP HLen! : " + std::to_string(mMessage.hlen));
        }
        mParsed.hAddrLen = mMessage.hlen;

        mParsed.hops = mMessage.hops;

        mParsed.xid = mMessage.xid;
        mParsed.secs = mMessage.secs;

        mParsed.broadcastFlag = (mMessage.flags & PktData::FLAG_BROADCAST) != 0;

        mParsed.ciAddr.setFrom(mMessage.ciAddr);
        mParsed.yiAddr.setFrom(mMessage.yiAddr);
        mParsed.siAddr.setFrom(mMessage.siAddr);
        mParsed.giAddr.setFrom(mMessage.giAddr);

        std::memcpy(mParsed.macAddr.ether_addr_octet, mMessage.chAddr, PktData::HLEN_ETHERNET);

        parseOptions();
    }

    void DhcpHeader::parseOptions()
    {
        // Reset optional settings to default.
        mParsed.resetOptions();

        // Find the length.
        mUsedOptionBytes = PktData::OPTIONS_ARR_SIZE;

        std::size_t start = sizeof(PktData::MAGIC_COOKIE);
        std::size_t end = PktData::OPTIONS_ARR_SIZE;

        // Check the magic cookie.
        if (std::memcmp(mMessage.options, PktData::MAGIC_COOKIE, sizeof(PktData::MAGIC_COOKIE)) != 0)
        {
            throw std::runtime_error("DHCP options do not contain correct magic cookie!");
        }

        for (auto idx = start; idx < end; )
        {
            auto optType = readOption<uint8_t>(idx);
            idx++;

            if (optType == 0xff)
            { // We found the end of the options array!
                mUsedOptionBytes = idx;
                return;
            }
            else if (optType == 0x00)
            { // Padding -> skip it.
                continue;
            }

            auto optLength = readOption<uint8_t>(idx);
            idx++;

            switch (optType)
            {
                case PktData::DHCP_OPTION_MSG_TYPE_ID :
                {
                    if (optLength != PktData::DHCP_OPTION_MSG_TYPE_LEN)
                    {
                        throw std::runtime_error("DHCP option message type has wrong length! : " + std::to_string(optLength));
                    }

                    auto msgType = readOption<uint8_t>(idx);

                    if (msgType < static_cast<uint16_t>(DhcpOptionType::Unknown))
                    {
                        mParsed.optType = static_cast<DhcpOptionType>(msgType);
                    }
                    else
                    {
                        mParsed.optType = DhcpOptionType::Unknown;
                    }

                    break;
                }
                case PktData::DHCP_OPTION_DHCP_SERV_ID :
                {
                    if (optLength != PktData::DHCP_OPTION_DHCP_SERV_LEN)
                    {
                        throw std::runtime_error("DHCP option server ID has wrong length! : " + std::to_string(optLength));
                    }

                    in_addr addr = {readOption<uint32_t>(idx)};
                    mParsed.serverAddr.setFrom(addr);

                    break;
                }
                case PktData::DHCP_OPTION_ADDR_REQ_ID :
                {
                    if (optLength != PktData::DHCP_OPTION_ADDR_REQ_LEN)
                    {
                        throw std::runtime_error("DHCP option requested IP has wrong length! : " + std::to_string(optLength));
                    }

                    in_addr addr = {readOption<uint32_t>(idx)};
                    mParsed.requestedAddr.setFrom(addr);

                    break;
                }
                default:
                {
                    break;
                }
            }

            // Skip the option type, option length and any following bytes.
            idx += optLength;
        }

        throw std::runtime_error("End block \"0xff\" has not been found in DHCP options array!");
    }

    void DhcpHeader::pushOptMsgType(uint8_t msgType)
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_MSG_TYPE_ID);
        pushOption<uint8_t>(PktData::DHCP_OPTION_MSG_TYPE_LEN);
        pushOption<uint8_t>(msgType);
    }

    void DhcpHeader::setOptMsgType(uint8_t msgType)
    {
        readOption<uint8_t>(MESSAGE_TYPE_POS) = msgType;
    }

    std::size_t DhcpHeader::pushOptAddrReq()
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_ADDR_REQ_ID);
        pushOption<uint8_t>(PktData::DHCP_OPTION_ADDR_REQ_LEN);
        // placeholder IP.
        pushOption<uint32_t>(0u);

        return mUsedOptionBytes - sizeof(uint32_t);
    }

    std::size_t DhcpHeader::pushOptServerId()
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_DHCP_SERV_ID);
        pushOption<uint8_t>(PktData::DHCP_OPTION_DHCP_SERV_LEN);
        // placeholder IP.
        pushOption<uint32_t>(0u);

        return mUsedOptionBytes - sizeof(uint32_t);
    }

    void DhcpHeader::pushOptRequestList(const std::initializer_list<uint8_t> &req)
    {
        if (req.size() > 255 || req.size() < 1)
        {
            throw std::runtime_error("Too many or too few request parameters!");
        }
        pushOption<uint8_t>(PktData::DHCP_OPTION_PARAM_REQ_ID);
        pushOption<uint8_t>(req.size());

        for (auto r : req)
        {
            pushOption<uint8_t>(r);
        }
    }

    void DhcpHeader::setOptIp(std::size_t idx, const Ipv4Address &addr)
    {
        readOption<uint32_t>(idx) = addr.getAddr();
    }

    void DhcpHeader::setMsgType(DhcpMessageType type)
    {
        switch (type)
        {
            case DhcpMessageType::Request:
            {
                mMessage.op = PktData::REQUEST;
                break;
            }
            case DhcpMessageType::Reply:
            {
                mMessage.op = PktData::REPLY;
                break;
            }
            default:
            {
                throw std::runtime_error("Cannot set unknown DHCP message type!");
            }
        }
    }

    void DhcpHeader::setOptType(DhcpOptionType type)
    {
        setOptMsgType(static_cast<uint8_t>(type));
    }

    void DhcpHeader::setHops(uint8_t hops)
    {
        mMessage.hops = hops;
        mParsed.hops = hops;
    }

    void DhcpHeader::setXid(uint32_t xid)
    {
        mMessage.xid = xid;
        mParsed.xid = xid;
    }

    void DhcpHeader::setSecs(uint16_t secs)
    {
        mMessage.secs = secs;
        mParsed.secs = secs;
    }

    void DhcpHeader::setBroadcast(bool flag)
    {
        // TODO - Ugly!
        if (!flag)
        {
            mMessage.flags &= ~PktData::FLAG_BROADCAST;
        }
        else
        {
            mMessage.flags |= PktData::FLAG_BROADCAST;
        }
    }

    void DhcpHeader::setCiAddr(const Ipv4Address &addr)
    {
        mMessage.ciAddr = *addr.address();
        mParsed.ciAddr = addr;
    }

    void DhcpHeader::setYiAddr(const Ipv4Address &addr)
    {
        mMessage.yiAddr = *addr.address();
        mParsed.yiAddr = addr;
    }

    void DhcpHeader::setSiAddr(const Ipv4Address &addr)
    {
        mMessage.siAddr = *addr.address();
        mParsed.siAddr = addr;
    }

    void DhcpHeader::setGiAddr(const Ipv4Address &addr)
    {
        mMessage.giAddr = *addr.address();
        mParsed.giAddr = addr;
    }

    void DhcpHeader::setHAddr(const ether_addr &addr)
    {
        std::memset(mMessage.chAddr, 0, PktData::CHADDR_ARR_SIZE);
        std::memcpy(mMessage.chAddr, addr.ether_addr_octet, ETH_ALEN);
        mParsed.macAddr = addr;
    }

    const uint8_t *DhcpHeader::getFinishBuffer()
    {
        // TODO - Ugly!
        *(mMessage.options + mUsedOptionBytes) = 0xff;

        return reinterpret_cast<uint8_t*>(&mMessage);
    }

    void DhcpHeader::pushOptLeaseTime(uint32_t leaseTime)
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_PARAM_REQ_LEASE_TIME);
        pushOption<uint8_t>(sizeof(uint32_t));
        pushOption<uint32_t>(htonl(leaseTime));
    }

    void DhcpHeader::pushOptSubnetMask(const Util::Ipv4Address &mask)
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_PARAM_REQ_SUBNET_MASK);
        pushOption<uint8_t>(PktData::DHCP_OPTION_IP_ADDR_LEN);
        pushOption<uint32_t>(mask.getAddr());
    }

    void DhcpHeader::pushOptRouter(const Util::Ipv4Address &addr)
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_PARAM_REQ_ROUTER);
        pushOption<uint8_t>(PktData::DHCP_OPTION_IP_ADDR_LEN);
        pushOption<uint32_t>(addr.getAddr());
    }

    void DhcpHeader::pushOptDns(const Util::Ipv4Address &addr)
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_PARAM_REQ_DNS);
        pushOption<uint8_t>(PktData::DHCP_OPTION_IP_ADDR_LEN);
        pushOption<uint32_t>(addr.getAddr());
    }

    void DhcpHeader::pushOptDomainName(const std::string &name)
    {
        pushOption<uint8_t>(PktData::DHCP_OPTION_PARAM_REQ_DOMAIN_NAME);
        if (name.length() > 255 || name.length() > (PktData::OPTIONS_ARR_SIZE - mUsedOptionBytes) || name.empty())
        {
            throw std::runtime_error("Domain name too long or empty! : \"" + name + "\"");
        }
        pushOption<uint8_t>(static_cast<uint8_t>(name.length()));
        for (auto c : name)
        {
            pushOption<uint8_t>(c);
        }
        /*
        for (auto it = name.rbegin(); it != name.rend(); ++it)
        {
            pushOption<uint8_t>(*it);
        }
         */
    }

    void DhcpParsedMessage::resetOptions()
    {
        optType = DhcpOptionType::Unknown;
        serverAddr.setFrom(0u);
        requestedAddr.setFrom(0u);
    }
}

std::ostream &operator<<(std::ostream &out, Util::DhcpMessageType type)
{
    switch (type)
    {
        case Util::DhcpMessageType::Request:
        {
            out << "Request";
            break;
        }
        case Util::DhcpMessageType::Reply:
        {
            out << "Reply";
            break;
        }
        default:
        {
            out << "Unknown";
            break;
        }
    }

    return out;
}

std::ostream &operator<<(std::ostream &out, Util::DhcpHardwareType type)
{
    switch (type)
    {
        case Util::DhcpHardwareType::Ethernet:
        {
            out << "Ethernet";
            break;
        }
        default:
        {
            out << "Unknown";
            break;
        }
    }
    return out;
}

std::ostream &operator<<(std::ostream &out, Util::DhcpOptionType type)
{
    switch (type)
    {
        case Util::DhcpOptionType::Discover:
        {
            out << "Discover";
            break;
        }
        case Util::DhcpOptionType::Offer:
        {
            out << "Offer";
            break;
        }
        case Util::DhcpOptionType::Request:
        {
            out << "Request";
            break;
        }
        case Util::DhcpOptionType::Ack:
        {
            out << "Ack";
            break;
        }
        case Util::DhcpOptionType::Decline:
        {
            out << "Decline";
            break;
        }
        case Util::DhcpOptionType::Nack:
        {
            out << "Nack";
            break;
        }
        case Util::DhcpOptionType::Release:
        {
            out << "Release";
            break;
        }
        case Util::DhcpOptionType::Inform:
        {
            out << "Inform";
            break;
        }
        default:
        {
            out << "Unknown";
            break;
        }
    }

    return out;
}

std::ostream &operator<<(std::ostream &out, const Util::DhcpParsedMessage &msg)
{
    out << "DHCP Message:"
        << "\n\tType: " << msg.type
        << "\n\tHType: " << msg.hType
        << "\n\tHLen: " << static_cast<uint16_t>(msg.hAddrLen)
        << "\n\thops: " << static_cast<uint16_t>(msg.hops)
        << "\n\tXID: " << std::hex << ntohl(msg.xid) << std::dec
        << "\n\tSecs: " << msg.secs
        << "\n\tBroadcast: " << (msg.broadcastFlag ? "Yes" : "No")
        << "\n\tCIAddr: " << msg.ciAddr
        << "\n\tYIAddr: " << msg.yiAddr
        << "\n\tSIAddr: " << msg.siAddr
        << "\n\tGIAddr: " << msg.giAddr
        << "\n\tHAddr: " << msg.macAddr
        << "\n\tOptType: " << msg.optType
        << "\n\tServer ID: " << msg.serverAddr
        << "\n\tRequested: " << msg.requestedAddr;

    return out;
}
