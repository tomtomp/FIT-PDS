/**
 * @file RogueApp.cpp
 * @author Tomas Polasek
 * @brief Application class for the DHCP rogue server application.
 */

#include "RogueApp.h"

RogueApp::RogueApp(int argc, char *const *argv) :
    mStarveXid{0u}
{
    parseInputParams(argc, argv);
}

RogueApp::~RogueApp()
{

}

int RogueApp::run()
{
    // DHCP server port.
    auto servEntry = getservbyname("bootps", "udp");
    if (!servEntry)
    {
        throw std::runtime_error("Unable to get \"bootps\" service entry!");
    }
    int servBootps{servEntry->s_port};

    // DHCP client port.
    servEntry = getservbyname("bootpc", "udp");
    if (!servEntry)
    {
        throw std::runtime_error("Unable to get \"bootpc\" service entry!");
    }
    int servBootpc{servEntry->s_port};

    // Initialize the IP address pool.
    Util::IpPool pool(mPoolSpec);
    pool.setLeaseTime(mLeaseTime);

    // Create the socket and bind it to requested interface.
    Util::SocketPcap sock(mTargetInterface, BUFFER_SIZE);
    // Setup filter for accepting only packets directed at port 67.
    sock.setFilter(DHCP_SERVER_FILTER, false, PCAP_NETMASK_UNKNOWN);

    // 0.0.0.0
    Util::Ipv4Address ipAny(0, 0, 0, 0);
    // 255.255.255.255
    Util::Ipv4Address ipBroadcast(255, 255, 255, 255);

    // TODO - What to do, when interface has no IP address?!?
    // Ip address and mask of target interface.
    auto address = Util::intIpv4Address(mTargetInterface);
    Util::Ipv4Address ipTargetInt(address);
    Util::Ipv4Address netTargetInt;
    Util::Ipv4Address maskTargetInt;
    sock.getIntNetMask(mTargetInterface, netTargetInt, maskTargetInt);

    // MAC address of target interface
    auto intMac = Util::intMACAddress(mTargetInterface);
    // FF:FF:FF:FF:FF:FF
    auto broadcastMac = ether_addr{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

    // Construct Ethernet header for broadcast frame.
    Util::EthHeader eth;
    eth.setSource(intMac);
    eth.setDst(broadcastMac);

    // Construct IP header for broadcast packet.
    Util::IpHeader ip;
    ip.setSource(ipTargetInt);
    ip.setDst(ipBroadcast);

    // Construct IP header for DHCP datagram.
    Util::UdpHeader udp;
    udp.setSource(servBootps);
    udp.setDst(servBootpc);

    // Construct DHCP header for not-acknowledge message.
    Util::DhcpHeader dhcpNack;
    dhcpNack.setMsgType(Util::DhcpMessageType::Reply);
    dhcpNack.setOptType(Util::DhcpOptionType::Nack);
    dhcpNack.setOptIp(dhcpNack.pushOptServerId(), ipTargetInt);

    // Construct DHCP header for offer message.
    Util::DhcpHeader dhcpOffer;
    dhcpOffer.setMsgType(Util::DhcpMessageType::Reply);
    dhcpOffer.setOptType(Util::DhcpOptionType::Offer);
    dhcpOffer.setSiAddr(ipTargetInt);
    dhcpOffer.setOptIp(dhcpOffer.pushOptServerId(), ipTargetInt);
    dhcpOffer.pushOptSubnetMask(maskTargetInt);
    dhcpOffer.pushOptRouter(mGateway);
    dhcpOffer.pushOptDns(mDns);
    dhcpOffer.pushOptDomainName(mDomain);
    dhcpOffer.pushOptLeaseTime(mLeaseTime);

    // Construct DHCP header for acknowledge message.
    Util::DhcpHeader dhcpAck;
    dhcpAck.setMsgType(Util::DhcpMessageType::Reply);
    dhcpAck.setOptType(Util::DhcpOptionType::Ack);
    dhcpAck.setSiAddr(ipTargetInt);
    dhcpAck.setOptIp(dhcpAck.pushOptServerId(), ipTargetInt);
    dhcpAck.pushOptSubnetMask(maskTargetInt);
    dhcpAck.pushOptRouter(mGateway);
    dhcpAck.pushOptDns(mDns);
    dhcpAck.pushOptDomainName(mDomain);
    dhcpAck.pushOptLeaseTime(mLeaseTime);

    // Combine headers into not-acknowledge packet.
    Util::Packet nackPkt(eth, ip, udp, dhcpNack);

    // Combine headers into offer packet.
    Util::Packet offerPkt(eth, ip, udp, dhcpOffer);

    // Combine headers into acknowledge packet.
    Util::Packet ackPkt(eth, ip, udp, dhcpAck);

    // Buffer for receiving DHCP packets.
    std::vector<uint8_t> buffer(Util::DhcpHeader::size());

    // Prepare parser for received answer from DHCP server.
    Util::DhcpHeader receivedPkt;

    // Start in the receive state.
    auto state = Stage::Receive;

    // Flag for printing receive message.
    bool printReceiveMsg{true};

    // Run until user decides to end the server.
    while (!Util::SigIntHandler::signalReceived() && state != Stage::Finished)
    {
        try {
            switch (state)
            {
                case Stage::Receive:
                { // Wait for packets aimed at us.
                    if (printReceiveMsg)
                    {
                        std::cout << "Waiting for message from client..." << std::endl;
                        printReceiveMsg = false;
                    }

                    sock.rcv(buffer);
                    receivedPkt.loadFrom(buffer, eth.size() + ip.size() + udp.size());
                    //std::cout << receivedPkt.info() << std::endl;

                    if (mStarveXid && receivedPkt.info().xid == mStarveXid)
                    {
                        std::cout << "Skipping DHCP messages from starve application!" << std::endl;
                        break;
                    }

                    switch (receivedPkt.info().optType)
                    {
                        case Util::DhcpOptionType::Discover :
                        { // We got the discover from potential client!
                            printReceiveMsg = true;
                            if (receivedPkt.info().siAddr != ipAny)
                            { // Server address should mbe set to 0.0.0.0 .
                                break;
                            }

                            std::cout << "Got discover from client :"
                                      << "\n\tmac :" << receivedPkt.info().macAddr
                                      << "\n\txid : 0x" << std::hex << ntohl(receivedPkt.info().xid)
                                      << std::dec << std::endl;

                            // Prepare offer packet.
                            dhcpOffer.setXid(receivedPkt.info().xid);
                            try {
                                // TODO - Shorted lease time for only offered addreses, full lease time after request.
                                dhcpOffer.setYiAddr(pool.leaseIp(receivedPkt.info().macAddr, receivedPkt.info().requestedAddr));
                            } catch (const std::runtime_error &err) {
                                // No more IP addresses available, stay in receive.
                                // state = Stage::Receive not required.
                                std::cerr << err.what() << " - Ignoring discover!" << std::endl;
                                break;
                            }
                            dhcpOffer.setHAddr(receivedPkt.info().macAddr);
                            offerPkt.replace(dhcpOffer);

                            // Send them our offer.
                            state = Stage::Offer;
                            break;
                        }

                        case Util::DhcpOptionType::Request :
                        { // Client is requesting IP address lease from us!
                            printReceiveMsg = true;
                            if (receivedPkt.info().siAddr.getAddr() && receivedPkt.info().siAddr != ipTargetInt)
                            { // Not targeted at us. 0.0.0.0 can mean a direct request for last address.
                                break;
                            }

                            std::cout << "Got request from client :"
                                      << "\n\tmac :" << receivedPkt.info().macAddr
                                      << "\n\txid : 0x" << std::hex << ntohl(receivedPkt.info().xid) << std::dec
                                      << "\n\trequested : " << receivedPkt.info().requestedAddr
                                      << std::dec << std::endl;

                            // Check if the IP address is free/belongs to the client.
                            if (pool.ipFree(receivedPkt.info().requestedAddr))
                            { // IP address is free to use by client.
                                pool.leaseIp(receivedPkt.info().macAddr);
                            }
                            else if (pool.ipBelongsTo(receivedPkt.info().requestedAddr, receivedPkt.info().macAddr))
                            { // Actually lease the IP address
                                // TODO - Set full lease time for requested IP address.
                                pool.renewIp(receivedPkt.info().requestedAddr);
                            }
                            else
                            { // Error, device requested IP address which is already used -> NACK!
                                dhcpNack.setHAddr(receivedPkt.info().macAddr);
                                nackPkt.replace(dhcpNack);
                                state = Stage::Nack;
                                break;
                            }

                            // Prepare acknowledge packet.
                            dhcpAck.setXid(receivedPkt.info().xid);
                            dhcpAck.setYiAddr(receivedPkt.info().requestedAddr);
                            dhcpAck.setHAddr(receivedPkt.info().macAddr);
                            ackPkt.replace(dhcpAck);

                            // Send the acknowledge.
                            state = Stage::Ack;
                            break;
                        }

                        default:
                        { // Other messages do not interest us.
                            break;
                        }
                    }

                    break;
                }
                case Stage::Offer:
                { // Sending the offer message to client.
                    std::cout << "Sending offer to client :"
                              << "\n\tmac :" << receivedPkt.info().macAddr
                              << "\n\txid : 0x" << std::hex << ntohl(receivedPkt.info().xid) << std::dec
                              << "\n\toffering : " << dhcpOffer.info().yiAddr << std::endl;
                    sock.send(offerPkt);

                    // Go back to receiving messages.
                    state = Stage::Receive;
                    break;
                }
                case Stage::Ack:
                { // Sending the acknowledge message to client.
                    std::cout << "Sending acknowledge to client :"
                              << "\n\tmac :" << receivedPkt.info().macAddr
                              << "\n\txid : 0x" << std::hex << ntohl(receivedPkt.info().xid) << std::dec
                              << "\n\tfor : " << dhcpAck.info().yiAddr << std::endl;
                    sock.send(ackPkt);

                    // Go back to receiving messages.
                    state = Stage::Receive;
                    break;
                }
                case Stage::Nack:
                { // Sending the not-acknowledged message to client.
                    std::cout << "Sending not-acknowledged to client :"
                              << "\n\tmac :" << receivedPkt.info().macAddr
                              << "\n\txid : 0x" << std::hex << ntohl(receivedPkt.info().xid) << std::dec
                              << "\n\tfor : " << dhcpAck.info().yiAddr << std::endl;
                    sock.send(nackPkt);

                    // Go back to receiving messages.
                    state = Stage::Receive;
                    break;
                }
                default:
                {
                    std::cerr << "Unknown application state, returning to receive..." << std::endl;
                    state = Stage::Receive;
                    break;
                }
            }
        } catch (const std::runtime_error &err) {
            //std::cerr << err.what() << std::endl;
        }
    }

    if (Util::SigIntHandler::signalReceived())
    {
        std::cout << "Received SIGINT, quitting!" << std::endl;
    }

    return EXIT_SUCCESS;
}

void RogueApp::parseInputParams(int argc, char *const *argv)
{
    std::string gateway;
    std::string dns;
    std::string lease;

    int c{0};
    while ((c = getopt(argc, argv, ARG_PARSE_OPTARG)) != -1)
    { // Parse input parameters.
        switch (c)
        {
            case 'i':
            { // Interface specification.
                if (optarg)
                {
                    mTargetInterface = optarg;
                }
                else
                {
                    throw std::runtime_error("Please specify interface with the \"-i\" parameter!");
                }

                break;
            }

            case 'p':
            { // Pool specification.
                if (optarg)
                {
                    mPoolSpec = optarg;
                }
                else
                {
                    throw std::runtime_error("Please specify pool with the \"-p\" parameter!");
                }

                break;
            }

            case 'g':
            { // Gateway specification.
                if (optarg)
                {
                    gateway = optarg;
                }
                else
                {
                    throw std::runtime_error("Please specify gateway with the \"-g\" parameter!");
                }

                break;
            }

            case 'n':
            { // DNS specification.
                if (optarg)
                {
                    dns = optarg;
                }
                else
                {
                    throw std::runtime_error("Please specify DNS with the \"-n\" parameter!");
                }

                break;
            }

            case 'd':
            { // Domain specification.
                if (optarg)
                {
                    mDomain = optarg;
                }
                else
                {
                    throw std::runtime_error("Please specify domain with the \"-d\" parameter!");
                }

                break;
            }

            case 'l':
            { // Lease time specification.
                if (optarg)
                {
                    lease = optarg;
                }
                else
                {
                    throw std::runtime_error("Please specify lease time with the \"-l\" parameter!");
                }

                break;
            }

            case 'x':
            { // Message XID.
                if (optarg)
                {
                    std::stringstream ss(optarg);
                    ss >> mStarveXid;
                    if (mStarveXid == 0)
                    {
                        throw std::runtime_error("Invalid starve XID, value must be numeric and greater than 0!");
                    }
                }
                else
                {
                    throw std::runtime_error("Please specify starve XID with the \"-x\" parameter!");
                }
                break;
            }

            default:
            { // Unknown parameter.
                throw std::runtime_error(ARG_PARSE_USAGE);
            }
        }
    }

    if (mTargetInterface.empty() || mPoolSpec.empty() || gateway.empty() || dns.empty() || mDomain.empty() || lease.empty())
    {
        throw std::runtime_error(ARG_PARSE_USAGE);
    }

    mGateway.setFrom(gateway);
    mDns.setFrom(dns);

    std::stringstream ss(lease);
    ss >> mLeaseTime;

    Util::SigIntHandler::setup();
}
