/**
 * @file Util.cpp
 * @author Tomas Polasek
 * @brief Utility functions and classes.
 */

#include "Util.h"

namespace Util
{
    in_addr intIpv4Address(const std::string &id)
    {
        ifaddrs *addrs{nullptr};
        getifaddrs(&addrs);
        ifaddrs *it{addrs};

        while (it != nullptr)
        {
            if ((id == it->ifa_name) && it->ifa_addr && (it->ifa_addr->sa_family == AF_INET))
            { // Looking for given interface and IPv4 address.
                return reinterpret_cast<sockaddr_in*>(it->ifa_addr)->sin_addr;
            }
            it = it->ifa_next;
        }

        if (addrs)
        {
            freeifaddrs(addrs);
        }

        throw std::runtime_error("Unable to get IP address for given interface: " + id);
    }

    ether_addr intMACAddress(const std::string &id)
    {
        ifaddrs *addrs{nullptr};
        getifaddrs(&addrs);
        ifaddrs *it{addrs};

        while (it != nullptr)
        {
            if ((id == it->ifa_name) && it->ifa_addr && (it->ifa_addr->sa_family == AF_PACKET))
            { // Looking for given interface and MAC address.
                return *reinterpret_cast<ether_addr*>(reinterpret_cast<sockaddr_ll*>(it->ifa_addr)->sll_addr);
            }
            it = it->ifa_next;
        }

        if (addrs)
        {
            freeifaddrs(addrs);
        }

        throw std::runtime_error("Unable to get MAC address for given interface: " + id);
    }

    ether_addr randomizeMacAddress(ether_addr addr)
    {
        // By Bill Lynch on stackoverflow.com
        static std::random_device rd;
        // Seed the RNG.
        static std::mt19937 rng(rd());

        return ether_addr{
            // Keep the OUI.
            static_cast<uint8_t>(addr.ether_addr_octet[0]),
            static_cast<uint8_t>(addr.ether_addr_octet[1]),
            static_cast<uint8_t>(addr.ether_addr_octet[2]),
            // "Randomize" the NIC.
            static_cast<uint8_t>(addr.ether_addr_octet[3] + rng()),
            static_cast<uint8_t>(addr.ether_addr_octet[4] + rng()),
            static_cast<uint8_t>(addr.ether_addr_octet[5] + rng())
        };
    }

    uint16_t checksum(uint16_t *addr, std::size_t len)
    {
        register std::size_t nleft = len;
        register uint16_t *w = addr;
        register int sum = 0;
        uint16_t answer = 0;

        /*
         * Our algorithm is simple, using a 32 bit accumulator (sum), we add
         * sequential 16 bit words to it, and at the end, fold back all the
         * carry bits from the top 16 bits into the lower 16 bits.
         */
        while (nleft > 1)  {
            sum += *w++;
            nleft -= 2;
        }

        /* mop up an odd byte, if necessary */
        if (nleft == 1) {
            *(uint8_t *)(&answer) = *(uint8_t *)w ;
            sum += answer;
        }

        /* add back carry outs from top 16 bits to low 16 bits */
        sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
        sum += (sum >> 16);         /* add carry */
        answer = ~sum;              /* truncate to 16 bits */
        return answer;
    }

    uint32_t intIndex(const std::string &id)
    {
        auto res = if_nametoindex(id.c_str());
        if (res == 0)
        {
            throw std::runtime_error("Unable to find interface! : " + id);
        }
        return res;
    }

    volatile bool SigIntHandler::sReceived{false};

    void SigIntHandler::setup()
    {
        if (signal(SIGINT, &SigIntHandler::signalHandler) == SIG_ERR)
        {
            std::cerr << "Unable to set SIGINT handler, application will not end "
                         "gracefully when receiving Ctrl+c!" << std::endl;
        }
    }

    volatile bool SigIntHandler::signalReceived()
    {
        return sReceived;
    }

    void SigIntHandler::signalHandler(int)
    {
        sReceived = true;
    }
}

std::ostream &operator<<(std::ostream &out, const ether_addr &addr)
{
    std::cout << std::hex << std::setw(2);
    for (uint16_t iii = 0; iii < 5; ++iii)
    {
        std::cout << static_cast<uint16_t>(addr.ether_addr_octet[iii]) << "::";
    }
    std::cout << static_cast<uint16_t>(addr.ether_addr_octet[5]) << std::dec;
    return out;
}
