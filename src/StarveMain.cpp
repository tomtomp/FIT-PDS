/**
 * @file StarveMain.cpp
 * @author Tomas Polasek
 * @brief Contains main function for the dhcp-starve application.
 */

#include "StarveMain.h"

int main(int argc, char * const argv[])
{
    try {
        StarveApp app(argc, argv);
        return app.run();
    } catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
    } catch (...) {
        std::cerr << "Unknown exception escaped the main loop!" << std::endl;
    }

    return EXIT_FAILURE;
}
