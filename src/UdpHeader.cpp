/**
 * @file UdpHeader.cpp
 * @author Tomas Polasek
 * @brief Udp header abstraction.
 */

#include "UdpHeader.h"

namespace Util
{
    UdpHeader::UdpHeader()
    {
        initialize();
    }

    void UdpHeader::initialize()
    {
        zero();

        mUdpHdr.source = HDR_PORT;
        mUdpHdr.dest = HDR_PORT;
        mUdpHdr.len = HDR_TOTAL_LENGTH;
        mUdpHdr.check = HDR_CHECK;
    }

    void UdpHeader::setPayloadLength(uint16_t len)
    {
        mUdpHdr.len = htons(size() + len);
    }

    void UdpHeader::setSource(uint16_t src)
    {
        mUdpHdr.source = src;
    }

    void UdpHeader::setDst(uint16_t dst)
    {
        mUdpHdr.dest = dst;
    }

    void UdpHeader::zero()
    {
        std::memset(rawData(), 0, size());
    }
}

