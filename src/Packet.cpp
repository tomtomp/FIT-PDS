/**
 * @file Packet.cpp
 * @author Tomas Polasek
 * @brief IP packet abstraction.
 */


#include "Packet.h"

namespace Util
{
    Packet::Packet(EthHeader &eth, IpHeader &ip, UdpHeader &udp, DhcpHeader &dhcp) :
        mDhcpPos{0u}
    {
        build(eth, ip, udp, dhcp);
    }

    void Packet::build(EthHeader &eth, IpHeader &ip, UdpHeader &udp, DhcpHeader &dhcp)
    {
        std::size_t bufferSize{eth.size() + ip.size() + udp.size() + dhcp.size()};
        mMsg.resize(bufferSize);

        ip.setPayloadLength(udp.size() + dhcp.size());
        udp.setPayloadLength(dhcp.size());

        std::size_t pos{0u};

        memcpy(rawData() + pos, eth.data(), eth.size());
        pos += eth.size();

        memcpy(rawData() + pos, ip.data(), ip.size());
        pos += ip.size();

        memcpy(rawData() + pos, udp.data(), udp.size());
        pos += udp.size();

        mDhcpPos = pos;
        memcpy(rawData() + pos, dhcp.getFinishBuffer(), dhcp.size());
        pos += dhcp.size();
    }

    void Packet::replace(DhcpHeader &dhcp)
    {
        if (mDhcpPos && (mDhcpPos + dhcp.size()) <= mMsg.size())
        {
            memcpy(rawData() + mDhcpPos, dhcp.getFinishBuffer(), dhcp.size());
        }
        else
        {
            throw std::runtime_error("Cannot replace non-existant DHCP header!");
        }
    }
}
