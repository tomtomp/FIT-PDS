/**
 * @file RogueMain.cpp
 * @author Tomas Polasek
 * @brief Main function for the dhcp rogue server application.
 */

#include "RogueMain.h"

int main(int argc, char * const argv[])
{
    try {
        RogueApp app(argc, argv);
        return app.run();
    } catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
    } catch (...) {
        std::cerr << "Unknown exception escaped the main loop!" << std::endl;
    }

    return EXIT_FAILURE;
}
