/**
 * @file EthHeader.cpp
 * @author Tomas Polasek
 * @brief Ethernet header abstraction.
 */

#include "EthHeader.h"

namespace Util
{
    EthHeader::EthHeader()
    {
        initialize();
    }

    void EthHeader::initialize()
    {
        zero();

        std::memset(mEthHdr.ether_dhost, HDR_ADDR_OCTET, sizeof(mEthHdr.ether_dhost));
        std::memset(mEthHdr.ether_shost, HDR_ADDR_OCTET, sizeof(mEthHdr.ether_shost));
        mEthHdr.ether_type = htons(HDR_TYPE);
    }

    void EthHeader::setSource(ether_addr &src)
    {
        std::memcpy(mEthHdr.ether_shost, src.ether_addr_octet, sizeof(mEthHdr.ether_shost));
    }

    void EthHeader::setDst(ether_addr &dst)
    {
        std::memcpy(mEthHdr.ether_dhost, dst.ether_addr_octet, sizeof(mEthHdr.ether_dhost));
    }

    void EthHeader::zero()
    {
        std::memset(rawData(), 0u, size());
    }
}
