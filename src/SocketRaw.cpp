/**
 * @file SocketRaw.cpp
 * @author Tomas Polasek
 * @brief Raw socket abstraction.
 */

#include "SocketRaw.h"

namespace Util
{

    SocketRaw::SocketRaw() :
        mCreated{false}, mHandle{-1}
    {
        create();
    }

    SocketRaw::~SocketRaw()
    {
        close();
    }

    SocketRaw::SocketRaw(SocketRaw &&other) :
        SocketRaw()
    {
        swap(std::forward<SocketRaw>(other));
    }

    SocketRaw &SocketRaw::operator=(SocketRaw &&rhs)
    {
        if (this != &rhs)
        {
            swap(std::forward<SocketRaw>(rhs));
        }

        return *this;
    }

    void SocketRaw::create()
    {
        close();

        // htons is required in this case?
        mHandle = socket(SOCK_DOMAIN, SOCK_TYPE, htons(SOCK_PROTOCOL));
        if (mHandle < 0)
        { // Test for error when creating the socket.
            throw std::runtime_error("Unable to create raw socket!");
        }

        mCreated = true;
    }

    void SocketRaw::bindInterface(const std::string &interface, bool inclIpHdr, std::size_t timeout)
    {
        ifreq intReq;
        memset(&intReq, 0, sizeof(intReq));
        memcpy(intReq.ifr_name, interface.data(), interface.size());

        int result = setsockopt(mHandle, SOL_SOCKET, SO_BINDTODEVICE,
                                &intReq, sizeof(intReq));
        if (result < 0)
        {
            throw std::runtime_error("Unable to setsockopt for binding to given interface: " + interface);
        }

        int flag{1u};

        result = setsockopt(mHandle, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
        if (result < 0)
        {
            throw std::runtime_error("Unable to setsockopt for reusable address!");
        }

        result = setsockopt(mHandle, SOL_SOCKET, SO_BROADCAST, &flag, sizeof(flag));
        if (result < 0)
        {
            throw std::runtime_error("Unable to setsockopt for broadcast!");
        }

        if (inclIpHdr)
        {
            result = setsockopt(mHandle, IPPROTO_IP, IP_HDRINCL, &flag, sizeof(flag));
            if (result < 0)
            {
                throw std::runtime_error("Unable to setsockopt for IP header included!");
            }
        }

        if (timeout)
        {
            timeval t;
            t.tv_sec = 0u;
            t.tv_usec = TIMEOUT_U_SECONDS;
            result = setsockopt(mHandle, SOL_SOCKET, SO_RCVTIMEO, &t, sizeof(t));
            if (result < 0)
            {
                throw std::runtime_error("Unable to setsockopt for receive timeout!");
            }
        }
    }

    void SocketRaw::sendTo(const Packet &msg, const Ipv4Address &to, in_port_t port)
    {
        sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_port = port;
        addr.sin_addr.s_addr = to.getAddr();
        memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

        if (sendto(mHandle, msg.data(), msg.size(), 0, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
        {
            throw std::runtime_error("Unable to send packet using raw socket! : " + std::to_string(errno) + " - " + strerror(errno));
        }
    }

    void SocketRaw::sendTo(const Packet &msg, std::uint32_t interface)
    {
        sockaddr_ll addr;
        memset(&addr, 0, sizeof(addr));
        addr.sll_ifindex = interface;

        if (sendto(mHandle, msg.data(), msg.size(), 0, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
        {
            throw std::runtime_error("Unable to send packet using raw socket! : " + std::to_string(errno) + " - " + strerror(errno));
        }
    }

    void SocketRaw::rcv(std::vector<uint8_t> &buffer)
    {
        if (::recv(mHandle, buffer.data(), buffer.size(), 0) < 0)
        {
            throw std::runtime_error("Unable to receive packet using raw socket! : " + std::to_string(errno) + " - " + strerror(errno));
        }
    }

    void SocketRaw::close()
    {
        if (mCreated)
        {
            ::close(mHandle);
            mHandle = -1;
            mCreated = false;
        }
    }

    void SocketRaw::swap(SocketRaw &&other)
    {
        std::swap(mHandle, other.mHandle);
        std::swap(mCreated, other.mCreated);
    }
}
