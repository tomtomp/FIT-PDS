/**
 * @file SocketPcap.cpp
 * @author Tomas Polasek
 * @brief Wrapper around pcap handle for ease of use.
 */

#include "SocketPcap.h"

namespace Util
{
    SocketPcap::SocketPcap(const std::string &interface, std::size_t bufferSize, std::size_t timeout) :
        SocketPcap()
    {
        create(interface, timeout);
    }

    SocketPcap::~SocketPcap()
    {
        close();
    }

    SocketPcap::SocketPcap(SocketPcap &&other) :
        SocketPcap()
    {
        swap(std::forward<SocketPcap>(other));
    }

    SocketPcap &SocketPcap::operator=(SocketPcap &&rhs)
    {
        if (this != &rhs)
        {
            swap(std::forward<SocketPcap>(rhs));
        }

        return *this;
    }

    void SocketPcap::create(const std::string &interface, std::size_t bufferSize, std::size_t timeout)
    {
        std::array<char, PCAP_ERRBUF_SIZE> errorBuffer;
        mHandle = pcap_create(interface.c_str(), errorBuffer.data());
        if (mHandle == nullptr)
        {
            throw std::runtime_error("Unable to create pcap handle! : " + std::string(errorBuffer.data()));
        }
        if (pcap_set_snaplen(mHandle, bufferSize))
        {
            std::string message(pcap_geterr(mHandle));
            close();
            throw std::runtime_error("Unable to set snaplen for pcap handle! : " + message);
        }
        if (pcap_set_promisc(mHandle, 1))
        {
            std::string message(pcap_geterr(mHandle));
            close();
            throw std::runtime_error("Unable to set promiscuous for pcap handle! : " + message);
        }
        if (timeout)
        {
            if (pcap_set_timeout(mHandle, timeout))
            {
                std::string message(pcap_geterr(mHandle));
                close();
                throw std::runtime_error("Unable to set timeout for pcap handle! : " + message);
            }
            if (pcap_set_immediate_mode(mHandle, 1))
            {
                std::string message(pcap_geterr(mHandle));
                close();
                throw std::runtime_error("Unable to set immediate mode for pcap handle! : " + message);
            }
        }
        if (pcap_activate(mHandle))
        {
            std::string message(pcap_geterr(mHandle));
            close();
            throw std::runtime_error("Unable to activate pcap handle! : " + message);
        }
    }

    void SocketPcap::setFilter(const std::string &filter, bool optimize, const Util::Ipv4Address &mask)
    {
        bpf_program prog;
        if (pcap_compile(mHandle, &prog, filter.c_str(), optimize, mask.getAddr()))
        {
            throw std::runtime_error("Unable to compile filter!" + std::string(pcap_geterr(mHandle)));
        }
        if (pcap_setfilter(mHandle, &prog))
        {
            throw std::runtime_error("Unable to set filter!" + std::string(pcap_geterr(mHandle)));
        }
    }

    void SocketPcap::send(const Packet &msg)
    {
        if (pcap_inject(mHandle, msg.data(), msg.size()) < 0)
        {
            throw std::runtime_error("Unable to send packet!");
        }
    }

    void SocketPcap::rcv(std::vector<uint8_t> &buffer)
    {
        pcap_pkthdr hdr;
        auto res = pcap_next(mHandle, &hdr);
        if (!res)
        {
            throw std::runtime_error("Unable to receive a packet!");
        }
        buffer.resize(hdr.caplen);
        buffer.assign(res, res + hdr.caplen);
    }

    void SocketPcap::close()
    {
        if (mHandle)
        {
            pcap_close(mHandle);
            mHandle = nullptr;
        }
    }

    void SocketPcap::swap(SocketPcap &&other)
    {
        std::swap(mHandle, other.mHandle);
    }

    void SocketPcap::getIntNetMask(const std::string &interface, Util::Ipv4Address &net, Util::Ipv4Address &mask)
    {
        std::array<char, PCAP_ERRBUF_SIZE> errorBuffer;
        in_addr netRes;
        in_addr maskRes;

        if (pcap_lookupnet(interface.c_str(), &netRes.s_addr, &maskRes.s_addr, errorBuffer.data()))
        {
            throw std::runtime_error("Unable to get IP and mask! : " + std::string(errorBuffer.data()));
        }

        net.setFrom(netRes);
        mask.setFrom(maskRes);
    }
}
