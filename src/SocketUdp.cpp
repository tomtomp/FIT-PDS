/**
 * @file SocketUdp.cpp
 * @author Tomas Polasek
 * @brief Udp socket abstractions.
 */

#include "SocketUdp.h"

namespace Util
{
    SocketUdp::SocketUdp() :
        mCreated{false}, mHandle{-1}
    {
        create();
    }

    SocketUdp::~SocketUdp()
    {
        close();
    }

    SocketUdp::SocketUdp(SocketUdp &&other) :
        SocketUdp()
    {
        swap(std::forward<SocketUdp>(other));
    }

    SocketUdp &SocketUdp::operator=(SocketUdp &&rhs)
    {
        if (this != &rhs)
        {
            swap(std::forward<SocketUdp>(rhs));
        }

        return *this;
    }

    void SocketUdp::create()
    {
        close();

        mHandle = socket(SOCK_DOMAIN, SOCK_TYPE, SOCK_PROTOCOL);
        if (mHandle < 0)
        { // Test for error when creating the socket.
            throw std::runtime_error("Unable to create udp socket!");
        }

        mCreated = true;
    }

    void SocketUdp::bindInterface(const std::string &interface, in_port_t port,
                                  std::size_t timeout)
    {
        sockaddr_in addr;
        std::memset(&addr, 0, sizeof(sockaddr_in));
        addr.sin_family = AF_INET;
        addr.sin_port = port;
        addr.sin_addr.s_addr = INADDR_ANY;

        /*
        auto address = Util::intIpv4Address(interface);
        Util::Ipv4Address target(address);
        addr.sin_addr = *target.address();
         */

        ifreq intReq;
        memset(&intReq, 0, sizeof(intReq));
        memcpy(intReq.ifr_name, interface.data(), interface.size());

        /*
        int result = setsockopt(mHandle, SOL_SOCKET, SO_BINDTODEVICE,
                                interface.c_str(), interface.length() + 1u);
                                */
        int result = setsockopt(mHandle, SOL_SOCKET, SO_BINDTODEVICE,
                                &intReq, sizeof(intReq));
        if (result < 0)
        {
            throw std::runtime_error("Unable to setsockopt for binding to given interface: " + interface);
        }

        int flag{1u};

        result = setsockopt(mHandle, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
        if (result < 0)
        {
            throw std::runtime_error("Unable to setsockopt for reusable address!");
        }

        result = setsockopt(mHandle, SOL_SOCKET, SO_BROADCAST, &flag, sizeof(flag));
        if (result < 0)
        {
            throw std::runtime_error("Unable to setsockopt for broadcast!");
        }

        if (timeout)
        {
            timeval t;
            t.tv_sec = 0u;
            t.tv_usec = TIMEOUT_U_SECONDS;
            result = setsockopt(mHandle, SOL_SOCKET, SO_RCVTIMEO, &t, sizeof(t));
            if (result < 0)
            {
                throw std::runtime_error("Unable to setsockopt for receive timeout!");
            }
        }

        result = bind(mHandle, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in));

        if (result < 0)
        {
            throw std::runtime_error("Unable to bind socket!");
        }
    }

    void SocketUdp::sendBroadcast(DhcpHeader &pkt, in_port_t port)
    {
        auto data = pkt.getFinishBuffer();

        sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_port = port;
        addr.sin_addr.s_addr = INADDR_BROADCAST;
        memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

        if (sendto(mHandle, data, pkt.size(), 0, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) < 0)
        {
            throw std::runtime_error("Unable to send broadcast packet! : " + std::to_string(errno) + " - " + strerror(errno));
        }
    }

    void SocketUdp::rcvBroadcast(std::vector<uint8_t> &buffer, in_port_t port)
    {
        /// Storage for address of received packet.
        sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        socklen_t addrLen{sizeof(sockaddr_in)};
        if (recvfrom(mHandle, buffer.data(), buffer.size(), 0, reinterpret_cast<sockaddr*>(&addr), &addrLen) < 0)
        {
            throw std::runtime_error("Unable to receive broadcast packet! : " + std::to_string(errno) + " - " + strerror(errno));
        }
    }

    void SocketUdp::close()
    {
        if (mCreated)
        {
            ::close(mHandle);
            mHandle = -1;
            mCreated = false;
        }
    }

    void SocketUdp::swap(SocketUdp &&other)
    {
        std::swap(mHandle, other.mHandle);
        std::swap(mCreated, other.mCreated);
    }
}

